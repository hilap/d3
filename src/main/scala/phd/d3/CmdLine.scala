package phd.d3

/**
 * Created by hila on 08/07/2015.
 */
import scopt._

case class Config(inputFile : String = "", objectClassifiedName : String = "", method1: String = "", method2: String = "", hydraIntegers : Boolean = false)
object CmdLine {
  val parser = new OptionParser[Config]("d3-solver") {
    head("d3-solver","1.0")
    help("help") text("Get solver usage")
    opt[String]('f', "input-file") required() valueName ("<input-file>") action ((x, c) => c.copy(inputFile = x)) text (
      "path to file containing the two methods")
    opt[String]('o', "object-name") required() valueName ("<object-name-fully-classified>") action ((x, c) =>
      c.copy(objectClassifiedName = x)) text ("fully classified name of the object that holds the methods")
    opt[String]('1', "method1") required() valueName ("<method-1>") action ((x, c) => c.copy(method1 = x)) text (
      "first method to compare"
      )
    opt[String]('2', "method2") required() valueName ("<method-2>") action ((x, c) => c.copy(method2 = x)) text (
      "second method to compare"
      )

    opt[Unit]('i',"hydra-ints") optional() action ((x,c) => c.copy(hydraIntegers = true)) text ("use hydra for integer refinement (intervals only)")

    checkConfig( c => if (new java.io.File(c.inputFile).exists()) success else failure("input file does not exist"))
  }
}
