package phd.d3

import com.typesafe.scalalogging.LazyLogging

import scala.collection.immutable.IndexedSeq

/**
 * Created by hila on 01/07/2015.
 */
class DisjunctJoinCalc[T, S <: Disjunct[T,S]] (val disjuncts: List[S], val cex : Iterable[T]) extends LazyLogging{
  def power[A](s: List[A]): List[List[A]] = {
    @annotation.tailrec
    def pwr(s: List[A], acc: List[List[A]]): List[List[A]] = s match {
      case Nil => acc
      case a :: as => pwr(as, acc ::: (acc map (a :: _)))
    }

    pwr(s, Nil :: Nil)
  }

  val precomputed = scala.collection.mutable.Map[Set[Int],S]()
//
//  def choose2List(tuples: Seq[(Set[Int])]) : Seq[((Set[Int]),(Set[Int]))] = {
//    for (i <- 0 until tuples.length;
//         j <- (i + 1) until tuples.length)
//      yield (tuples(i),tuples(j))
//
//  }

  def choose2AsStream(tuples: Seq[Set[Int]], first : Int, second : Int) : Stream[(Set[Int],Set[Int])] =
    if (first >= tuples.length) Stream.empty
    else if (second >= tuples.length) choose2AsStream(tuples,first+1,first+2)
    else Stream.cons((tuples(first),tuples(second)),choose2AsStream(tuples,first,second+1))

  def choose2(tuples : Seq[Set[Int]]) = choose2AsStream(tuples,0,1)

  def precompute(/*s : Set[Int]*/) : Unit = {
//try 3
    var prevLvl = (0 until disjuncts.length).map(i => Set(i))
    prevLvl.foreach( i => precomputed.put(i,disjuncts(i.head)))
    for (level <- 2 to disjuncts.length){
      val currLevel = scala.collection.mutable.Map[Set[Int],Boolean]()
      for ((subset1,subset2) <- choose2(prevLvl);
           if (subset1 ++ subset2).size == level){
        if (!currLevel.contains(subset1 ++ subset2)) {
          val p1 = precomputed(subset1)
          val p2 = precomputed(subset2)
          if (p1.canJoin && p2.canJoin) {
            val j = p1.join(p2)
            if (cex.forall(c => !j.includes(c))) {
              currLevel.put(subset1 ++ subset2, true)
              precomputed.put(subset1 ++ subset2, j)
            }
            else
              currLevel.put(subset1 ++ subset2, false)
          }
        }
      }
      prevLvl = currLevel.filter(_._2).map(_._1).toIndexedSeq
    }

//try 2
//    (0 until disjuncts.length).foreach(i => precomputed.put(Set(i),disjuncts(i)))
//    for (level <- 2 to disjuncts.length){
//      val lvlPrecomputed = scala.collection.mutable.Map[Set[Int],S]()
//      for (subset <- precomputed.keys.filter(k => k.size == level - 1);
//           i <- (0 until disjuncts.length).toSet -- subset) {
//        if (!lvlPrecomputed.contains(subset + i)) {
//          val a = disjuncts(i)
//          val b = precomputed(subset)
//          val j = a.join(b)
//          if (cex.forall(c => !j.includes(c)))
//            lvlPrecomputed.put(subset + i, j)
//        }
//
//      }
//      precomputed ++= lvlPrecomputed
//    }

//    if (precomputed.contains(s)) return
//    if (s.size == 1)
//      precomputed.put(s.toSet, Some(disjuncts(s.head)))
//    else{
//      s.foreach{ i => precompute(s - i) }
//      if (s.forall(i =>
//        !precomputed(s-i).isEmpty)) {
//        val a = precomputed(Set(s.head)).get
//        val b = precomputed(s.tail).get
//        val j = a.join(b)
//        if (cex.forall(c => !j.includes(c)))
//          precomputed.put(s.toSet, Some(j))
//        else
//          precomputed.put(s.toSet,None)
//      }
//      else precomputed.put(s.toSet,None)
//    }
  }

  def do_join() : List[S] = {
    if (disjuncts.length <= 1) return disjuncts

    if (disjuncts.length >= 20) logger.warn("Attempting to join " + disjuncts.length + " disjuncts")
    precompute(/*(0 until disjuncts.length).toSet*/)

    //now selection:
    //val subsets = precomputed.keys.toSeq.sortWith((lhs,rhs) => lhs.length > rhs.length)
    //val it = subsets.iterator
    val seen = scala.collection.mutable.Set[Int]()
    val res = scala.collection.mutable.Set[S]()
    while (!(0 until disjuncts.length).forall(d => seen.contains(d))) {
      val mxSize = precomputed.keys.filter(s => s.intersect(seen).isEmpty).groupBy(s => s.size).maxBy(_._1)._2
      if (mxSize.head.size == 1) //we're down to only singletons
      {
        mxSize.foreach{ s =>
          s.foreach(d => seen.add(d))
          res.add(precomputed(s))
        }
      }
      else {
        val s: Set[Int] = if (mxSize.size == 1)
          mxSize.head
        else
          mxSize.maxBy(s => precomputed(s).cardinality(s.map(i => disjuncts(i))))
        s.foreach(d => seen.add(d))
        res.add(precomputed(s))
      }
    }
    res.toList
  }
}
