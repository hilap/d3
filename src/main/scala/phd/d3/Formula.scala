package phd.d3
import phd.d3.DisjunctJoinCalc
/**
 * Created by hila on 09/06/2015.
 */

trait Disjunct[T, S <: Disjunct[T,S]] {
  def includes(x : T) : Boolean
  def isEmpty : Boolean
  def isEquivalent (x : S) : Boolean

  def subsetOf (rhs : S) : Boolean
  def subsetEq (rhs : S) : Boolean

  val canJoin : Boolean
  def join(other: S) : S

  def cardinality(originatingSet : Set[S]) : BigInt = originatingSet.size
}

trait Formula[T, U <: Formula[T,U]] {
  //returns true if x satisfies this formula
  def includes(x: T): Boolean

  def isEmpty: Boolean

  def isTautology : Boolean

  def except(xs: Iterable[T]): U

  def including(xs: Iterable[T]): U

  def ||(rhs: U): U

  def &&(rhs: U): U

  def unary_! : U
}

trait DNF[T, S <: Disjunct[T,S], U <: DNF[T,S,U,F], F <: Formula[T,F]]{
  val disjuncts: List[S]

  def asFormula : F
  //returns true if x satisfies this formula
  def includes(x: T): Boolean = !disjuncts.forall(d => !d.includes(x))

  def isEmpty: Boolean = disjuncts.forall(d => d.isEmpty)

  def except(xs: Iterable[T]): F

  def including(xs: Iterable[T]): U

  def ||(rhs: F): F

  def &&(rhs: F): F

  def unary_! : F

  def power[A](s: List[A]): List[List[A]] = {
    @annotation.tailrec
    def pwr(s: List[A], acc: List[List[A]]): List[List[A]] = s match {
      case Nil => acc
      case a :: as => pwr(as, acc ::: (acc map (a :: _)))
    }

    pwr(s, Nil :: Nil)
  }


  def joinDisjuncts(positiveEx : Iterable[T], counterEx : Iterable[T])(implicit disjunctUnitCtor: (T) => S, newFormula : (List[S]) => U) : U = {
    if (disjuncts.isEmpty) return newFormula(disjuncts)

    val (disproved,hold) = disjuncts.partition(d => !counterEx.filter(c => d.includes(c)).isEmpty)
    val concrete = disproved.flatMap(d => positiveEx.filter(c => d.includes(c)))
    val tmp : List[S] = hold ++ concrete.map(c => disjunctUnitCtor(c))
    val calc = new DisjunctJoinCalc[T,S](tmp,counterEx)
    val j = newFormula(calc.do_join())
    assert(positiveEx.filter(c => !j.includes(c)).isEmpty)
    j
  }

  def refineRemoving(toRemove: Iterable[T], concreteBase: Iterable[T],cex: Iterable[T])(implicit disjunctUnitCtor: (T) => S, newFormula : (List[S]) => U) : U = {
    val (stay,go) = disjuncts.partition(d => toRemove.filter(x => d.includes(x)).isEmpty)
    val newDisj = go.map(d => concreteBase.filter(x => d.includes(x) && stay.filter(s => s.includes(x)).isEmpty).map(disjunctUnitCtor)).flatMap(cs => new DisjunctJoinCalc(cs.toList,cex).do_join())
    newFormula(newDisj ++ stay)
  }
}

trait Hydrable[T,S <: Hydrable[T,S]] extends Disjunct[T,S] {
  def meet(other : S) : S
  def =< (other : S) : Boolean
  def isBottom : Boolean
}

trait HydrableDNF[T, S <: Hydrable[T,S], U <: HydrableDNF[T,S,U,F], F <: Formula[T,F]] extends DNF[T,S,U,F] {
  def hydrableRefineRemoving(toRemove: Iterable[T], regionsPartition: U, concreteBase: Iterable[T],cex: Iterable[T])(implicit disjunctUnitCtor: (T) => S, newFormula : (List[S]) => U) : U = {
    if (disjuncts.isEmpty) return newFormula(disjuncts)
    val (stay,go) = disjuncts.partition(d => toRemove.filter(x => d.includes(x)).isEmpty)
    if (go.isEmpty) return newFormula(stay)
    val goPts = concreteBase.filter(c => go.exists(_.includes(c)))
    val newDisj = regionsPartition.disjuncts.flatMap{p =>
      val pts = goPts.filter(p.includes(_))
      //Hail Hydra
      if (pts.isEmpty) None
      else Some(pts.foldLeft(disjunctUnitCtor(pts.head)){(z,f) => z join disjunctUnitCtor(f)})
    }
    val n = newFormula(newDisj ++ stay)
    assert(concreteBase.filter(!n.includes(_)).isEmpty)
    n
  }
  def hydrableJoinDisjuncts(regionsPartition: U, positiveEx : Iterable[T], counterEx : Iterable[T])(implicit disjunctUnitCtor: (T) => S, newFormula : (List[S]) => U) : U = {
    if (disjuncts.isEmpty) return newFormula(disjuncts)

    val (disproved,hold) = disjuncts.partition(d => !counterEx.filter(c => d.includes(c)).isEmpty)
    val concrete = disproved.flatMap(d => positiveEx.filter(c => d.includes(c)))
    val tmp : List[S] = hold ++ concrete.map(c => disjunctUnitCtor(c))

    //Hail Hydra
    val j : List[S] = regionsPartition.disjuncts.flatMap { p =>
      val t = tmp.filter(_.subsetEq(p))
      if (t.isEmpty) None
      else Some(t.foldLeft(t.head)((z,f) => z join f))
    }

    assert(positiveEx.filter(c => !j.includes(c)).isEmpty)
    j
  }

  def meet(rhs : U)(implicit newFormula : (List[S]) => U) : U = {
    val productMeets = for( x <- this.disjuncts; y <- rhs.disjuncts) yield x meet y
    productMeets.filter(!_.isBottom)
  }
}