package phd.d3

import java.lang.Class

import com.typesafe.scalalogging.LazyLogging
import main.scala.phd.d3.arrays.{ArrayPredicates, ArrayPredicatesFormula, ArrayPredicatesDNF}
import main.scala.phd.d3.integers.{IntegerBoxesDNF, IntegerBoxesFormula, IntegerBox}
import phd.d3.WhiteboxOracle.{Whitebox2DOracle, WhiteBoxIntArrayOracle, WhiteboxIntOracle}
import phd.d3.integers._

import scala.concurrent.{Future, Await}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.TimeoutException
import scala.util.{Try,Success,Failure}
import scala.reflect.runtime.universe._

object Main extends LazyLogging{

  implicit def int2String(x : Int) = x.toString
  implicit def arr2String(x : Array[Int]) = x.mkString("[",",","]")
  implicit def tup2String(xy : (Int,Int)) = xy.toString

  object Compiler extends Enumeration {
    type Compiler = Value
    val ScalaC = Value("SCALA")
    val JavaC = Value("JAVA")
  }
  import Compiler._

  def main(args: Array[String]) {
    val cfg = CmdLine.parser.parse(args, Config()) match {
      case None => return
      case Some(config) => config
      // arguments are bad, error message will have been displayed
    }

    val compiler: Compiler.Compiler = cfg.inputFile.substring(cfg.inputFile.lastIndexOf('.')) match {
      case ".scala" => ScalaC
      case ".java" => ??? //JavaC
      case _ => ???
    }

    //1. read snippet
    val obj = new ObjectFromFile(cfg.inputFile, cfg.objectClassifiedName, List(cfg.method1, cfg.method2), compiler)

    //2. compile program snippet
    assert(obj.params.toSet.size == 1)
    if (obj.params.head == 0.getClass) {
      if (obj.params.length == 1) {
        val a = (in: Int) => obj(0, List(in.asInstanceOf[AnyRef]))
        val b = (in: Int) => obj(1, List(in.asInstanceOf[AnyRef]))

        val oracle = //new IntegersOracle
          new WhiteboxIntOracle(scala.io.Source.fromFile(cfg.inputFile).mkString)

        if (cfg.hydraIntegers)
          //Hail Hydra!
          Hydra.do_hydrable_d3[Int,Interval,IntervalsDisjunct,IntervalsDisjunct](IntervalsDisjunct(List(Interval(Int.MinValue,Int.MaxValue))),IntervalsDisjunct(List()),oracle,a,b)
        else
          do_main[Int, IntegerPredicates, IntegerPredicatesFormula, IntegerPredicatesDNF](IntegerPredicatesDNF(List()), IntegerPredicatesDNF(List()), oracle, a, b)
        //Hydra.do_hydra[Int,Interval,IntervalsDisjunct](Interval(Int.MinValue,Int.MaxValue),Interval(1,0),oracle,a,b)


      }
      else {
        assert(obj.params.length == 2)
        val a = (args: (Int, Int)) => obj(0, List(args._1.asInstanceOf[AnyRef],args._2.asInstanceOf[AnyRef]))
        val b = (args: (Int, Int)) => obj(1, List(args._1.asInstanceOf[AnyRef],args._2.asInstanceOf[AnyRef]))

        val oracle =
          new Whitebox2DOracle(scala.io.Source.fromFile(cfg.inputFile).mkString)

        do_main[(Int,Int), IntegerBox, IntegerBoxesFormula, IntegerBoxesDNF](new IntegerBoxesDNF(List()), new IntegerBoxesDNF(List()), oracle, a, b)
      }
    }
    else {
      assert(obj.params.head.isArray)
      val a = (in: Array[Int]) => obj(0, List(in.asInstanceOf[AnyRef]))
      val b = (in: Array[Int]) => obj(1, List(in.asInstanceOf[AnyRef]))
      val oracle = new WhiteBoxIntArrayOracle(scala.io.Source.fromFile(cfg.inputFile).mkString)
      ArrayPredicates.specialValues = oracle.specialValues.toSeq
      do_main[Array[Int],ArrayPredicates,ArrayPredicatesFormula,ArrayPredicatesDNF](
        ArrayPredicatesDNF(List()),
        ArrayPredicatesDNF(List()), oracle, a, b)

    }
  }

  def do_main[T,S <: Disjunct[T,S], F <: Formula[T,F] : TypeTag, D <: DNF[T,S,D,F]](same_init : D, diff_init : D, oracle : Oracle[T], a : (T => Any), b : (T => Any))
                                                               (implicit disjunctUnitCtor: (T) => S, newFormula : (List[S]) => D, print : (T) => String){
  var iter = 1
    var phi_same = same_init
    var phi_diff = diff_init
    var phi_s = same_init
    var phi_ng = diff_init
    var c_same = Set[T]()
    var c_diff = Set[T]()

    logger.info("Starting...")
    while (!((phi_same || phi_diff.asFormula).isTautology) || !((phi_same && phi_diff.asFormula).isEmpty)) {
      //3. get inputs from oracle
      val disagreementRegion = phi_same && phi_diff.asFormula
      val coverage = phi_same || phi_diff.asFormula

      //consistency check inputs
      val c_pos = oracle.getInputFrom(phi_s.asFormula)
      val c_neg = oracle.getInputFrom(phi_ng.asFormula)
      logger.info("sampled consistency checks, got " + c_pos.map(print) + "," + c_neg.map(print))
      if (c_neg.map(phi_s.includes).getOrElse(false) || c_pos.map(phi_ng.includes).getOrElse(false)) {
        //abort
        println("Failed consistency")
        return
      }
      else {
        c_same = c_same ++ List(c_pos).flatten
        c_diff = c_same ++ List(c_neg).flatten

      }

      val inputs : List[T] =
        {
          val c_1 = oracle.getInputFrom(phi_same.asFormula)
          logger.info("Sampled phi_same, got " + c_1.map(print))
          val c_2 = oracle.getInputFrom(phi_diff.asFormula)
          logger.info("Sampled phi_diff, got " + c_2.map(print))
          val c_3 = oracle.getInputFrom(!coverage)
          logger.info("Sampled uncovered, got " + c_3.map(print))
          val c_4 = oracle.getInputFrom(disagreementRegion)
          logger.info("Sampled covered disagreement, got " + c_4.map(print))

          List(c_1, c_2, c_3, c_4,c_pos,c_neg)
        }
      .flatten
      if (inputs.isEmpty) ???

      //4. get inputs where outputs are different
      val outputs = inputs.map( i => Tuple3(i,Result(i,a), Result(i,b)))
      val diffs = outputs.filter(o => o._2 != o._3).map(o => o._1)
      val same = outputs.filter(o => o._2 == o._3).map(o => o._1)
      logger.info("Tested samples, same = " + same.map(print) + ", diff = " + diffs.map(print))
      c_same = c_same ++ same
      c_diff = c_diff ++ diffs

      phi_s = phi_s.including(same)
      phi_ng = phi_ng.including(diffs)

      //\foreach c \in C_same add the concrete cases
      phi_same = phi_same.including(same.filter(!phi_same.includes(_)))
      phi_diff = phi_diff.refineRemoving(same.filter(phi_diff.includes),c_diff,c_same)

      logger.info("Added c\\in C_same, phi_same now has " + phi_same.disjuncts.length + " disjuncts")
      logger.info("Collapsed contradictions for c\\in C_same, phi_diff now has " + phi_diff.disjuncts.length + " disjuncts")

      //\foreach c \in C_diff add the concrete cases
      phi_diff  = phi_diff.including(diffs.filter(!phi_diff.includes(_)))
      phi_same = phi_same.refineRemoving(diffs.filter(phi_same.includes),c_same,c_diff)
      logger.info("Added c\\in C_diff, phi_diff now has " + phi_diff.disjuncts.length + " disjuncts")
      logger.info("Collapsed contradictions for c\\in C_diff, phi_same now has " + phi_same.disjuncts.length + " disjuncts")

      phi_diff = phi_diff.joinDisjuncts(c_diff,c_same)
      logger.info("After join phi_diff has " + phi_diff.disjuncts.length + " disjuncts")
      phi_same = phi_same.joinDisjuncts(c_same,c_diff)
      logger.info("After join phi_same has " + phi_same.disjuncts.length + " disjuncts")

      println("iter = " + iter)
      println("C = " + inputs.map(print))
      println("same =" + phi_same)
      println("diff =" + phi_diff)
      iter += 1
    }

    println("Finished;")
    println("c_same =" + c_same)
    println("c_diff =" + c_diff)
  }
}

object Hydra extends LazyLogging{
  def do_hydra[T, S <: Hydrable[T,S] with Ordered[S], U <: Formula[T,U] : TypeTag](top : S, bottom : S, oracle : Oracle[T], a : (T => Any), b : (T => Any))
                                                                                  (implicit beta : (T) => S, alphaMinus : (Set[T]) => Seq[S], toFormula: (List[S]) => U) = {
    val upperBounds = scala.collection.mutable.Map[S,S](top -> bottom)

    var c_same = Set[T]()
    var c_diff = Set[T]()
    var iter = 0

    while(upperBounds.exists(b => b._1 != b._2)) {
      iter = iter + 1
      val in : T = oracle.getInputFrom(toFormula(upperBounds.keys.toList)).get
      if (Result(in,a) == Result(in,b)) { //pos ex
        c_same = c_same + in
        for(ub <- upperBounds; if ub._1.includes(in)) {
          val descriminantDesc = ub._2
          val joinRes = descriminantDesc.join(beta(in))
          if (joinRes =< ub._1)
            upperBounds.update(ub._1,joinRes)
          else
            ??? //oevershoot in a discriminant. What does this mean?)
        }
      }
      else //neg ex
      {
        c_diff = c_diff + in
        for ((ub,desc) <- upperBounds; if ub.includes(in)) { //should be at most one of these
        val newUB = alphaMinus(Set(in)).map(_ meet ub).filter(!_.isBottom)
          val (leq,notleq) = newUB.partition(desc =< _)
          assert(leq.size <= 1)
          upperBounds.remove(ub)
          if (!leq.isEmpty) {
            upperBounds.put(leq.head, desc)
            notleq.filter(!_.isBottom).foreach(upperBounds.put(_,bottom))
          }
          else for (u <- newUB) {
            upperBounds.put(u, c_same.filter(u.includes(_)).foldLeft(bottom)((z,f) => z join beta(f)))
          }
        }
      }

      val violating = upperBounds.filter(ub => c_diff.exists(c => ub._2.includes(c))).toList
      //split all violators, recalc their lbs.
      for ((ub,desc) <- violating) {
        val regionPlayers = c_same.filter(c => ub.includes(c))
        val toMeet : Seq[S] = alphaMinus(c_diff.filter(desc.includes(_)))
        val newUB : Seq[S] = toMeet.map( _ meet ub).filter(!_.isBottom)
        val grouped = regionPlayers.groupBy(c => newUB.filter(u => u.includes(c)).headOption)
        assert(!grouped.contains(None))
        upperBounds.remove(ub)
        //for((gr,pts) <- grouped)
        //  upperBounds.put(gr.get, pts.foldLeft(beta(pts.head))((z,f) => z.join(beta(f))))
        //Hydra says: leave in even regions with no positive examples, you may need them.
        for (u <- newUB) {
          upperBounds.put(u, if (grouped.contains(Some(u)))
            grouped(Some(u)).foldLeft(bottom)((z,f) => z.join(beta(f)))
          else
            bottom
          )
        }
      }
      println(s"iter=$iter")
      println(s"in=$in")
      println("partition=" + upperBounds.keys.toList.sorted.mkString("||"))
      println("desc=" + upperBounds.values.filter(!_.isBottom).toList.sorted.mkString(" || "))
    }
    println("Hail hydra!")
  }

  def do_hydrable_d3[T, S <: Hydrable[T,S] with Ordered[S], U <: Formula[T,U] : TypeTag, D <: HydrableDNF[T,S,D,U]](top_dnf : D, bottom_dnf : D, oracle : Oracle[T], a : (T => Any), b : (T => Any))
                                                                                      (implicit beta : (T) => S, alphaMinus : (Set[T]) => Seq[S], toFormula: (List[S]) => D) = {

    var iter = 1
    var phi_same = bottom_dnf
    var phi_diff = bottom_dnf
    var same_regions = top_dnf
    var diff_regions = top_dnf
    var c_same = Set[T]()
    var c_diff = Set[T]()

    logger.info("Starting...")
    while (!((phi_same || phi_diff.asFormula).isTautology) || !((phi_same && phi_diff.asFormula).isEmpty)) {
      //3. get inputs from oracle
      val disagreementRegion = phi_same && phi_diff.asFormula
      val coverage = phi_same || phi_diff.asFormula


      val inputs : List[T] =
        {
          val c_1 = oracle.getInputFrom(phi_same.asFormula)
          logger.info("Sampled phi_same, got " + c_1.map(print))
          val c_2 = oracle.getInputFrom(phi_diff.asFormula)
          logger.info("Sampled phi_diff, got " + c_2.map(print))
          val c_3 = oracle.getInputFrom(!coverage)
          logger.info("Sampled uncovered, got " + c_3.map(print))
          val c_4 = oracle.getInputFrom(disagreementRegion)
          logger.info("Sampled covered disagreement, got " + c_4.map(print))
          List(c_1, c_2, c_3, c_4)
        }
          .flatten
      if (inputs.isEmpty) ???

      //4. get inputs where outputs are different
      val outputs = inputs.map( i => Tuple3(i,Result(i,a), Result(i,b)))
      val diffs = outputs.filter(o => o._2 != o._3).map(o => o._1)
      val same = outputs.filter(o => o._2 == o._3).map(o => o._1)
      logger.info("Tested samples, same = " + same.map(print) + ", diff = " + diffs.map(print))
      c_same = c_same ++ same
      c_diff = c_diff ++ diffs

      diff_regions = diff_regions.meet(alphaMinus(same.toSet).toList)
      same_regions = same_regions.meet(alphaMinus(diffs.toSet).toList)

      //\foreach c \in C_same add the concrete cases
      phi_same = phi_same.including(same.filter(!phi_same.includes(_)))
      logger.info("Added c\\in C_same, phi_same now has " + phi_same.disjuncts.length + " disjuncts")
      phi_diff  = phi_diff.including(diffs.filter(!phi_diff.includes(_)))
      logger.info("Added c\\in C_diff, phi_diff now has " + phi_diff.disjuncts.length + " disjuncts")

      phi_diff = phi_diff.hydrableRefineRemoving(same.filter(phi_diff.includes),diff_regions, c_diff,c_same)
      logger.info("Collapsed contradictions for c\\in C_same, phi_diff now has " + phi_diff.disjuncts.length + " disjuncts")

      //\foreach c \in C_diff add the concrete cases

      phi_same = phi_same.hydrableRefineRemoving(diffs.filter(phi_same.includes),same_regions,c_same,c_diff)
      logger.info("Collapsed contradictions for c\\in C_diff, phi_same now has " + phi_same.disjuncts.length + " disjuncts")

      phi_diff = phi_diff.hydrableJoinDisjuncts(diff_regions,c_diff,c_same)
      logger.info("After join phi_diff has " + phi_diff.disjuncts.length + " disjuncts")
      phi_same = phi_same.hydrableJoinDisjuncts(same_regions,c_same,c_diff)
      logger.info("After join phi_same has " + phi_same.disjuncts.length + " disjuncts")

      println("iter = " + iter)
      println("C = " + inputs.map(print))
      println("same =" + phi_same)
      println("diff =" + phi_diff)
      iter += 1
    }

    println("Finished;")
    println("c_same =" + c_same)
    println("c_diff =" + c_diff)
  }
}