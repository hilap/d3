package phd.d3

import scala.tools.nsc._
import java.io.File

/**
 * Created by hila on 07/07/2015.
 */
class ObjectFromFile(filePath : String, className : String, methodNames: List[String], compiler : Main.Compiler.Compiler) {
  assert(new File(filePath).exists())

  val cls = {
    val scalaJar = System.getProperty("java.class.path").split(java.io.File.pathSeparator)
      .filter {
      _.contains("scala-library")
    }.head

    val settings = new Settings()
    settings.bootclasspath.append(scalaJar)
    settings.classpath.append(scalaJar)
    val g = new Global(settings)
    val run = new g.Run
    run.compile(List(filePath))
    val classLoader = new java.net.URLClassLoader(
      Array(new File(".").toURI.toURL), // Using current directory.
      this.getClass.getClassLoader)

    classLoader.loadClass(className + "$") // load class
  }

  val obj = cls.getField("MODULE$").get(null)
  val ms = cls.getDeclaredMethods().filter(m => methodNames.contains(m.getName)).toList
  val params = {
    val ps = ms.map(m => m.getParameterTypes)
    assert(ps.forall(p => p.length == ps(0).length && (1 until p.length).forall(i => p(i) == ps(0)(i))))
    ps(0)
  }

  def numMethods = ms.length
  def apply(idx : Int, params : Iterable[AnyRef]) : AnyRef = ms(idx).invoke(obj,params.toArray[Object]:_*)
}
