package phd.d3

import com.microsoft.z3.{IntExpr, IntNum, Status}
import com.typesafe.scalalogging.LazyLogging
import main.scala.phd.d3.arrays.ArrayPredicatesFormula
import main.scala.phd.d3.integers.{BoxSolverOps, IntegerBoxesFormula}
import org.scalacheck.{Gen, Arbitrary}
import org.scalacheck.Gen.Parameters
import phd.d3.{Formula, DNF}
import phd.d3.integers._

import scala.reflect.runtime.universe._
import scala.util.Random
import java.util.concurrent.TimeoutException

trait Oracle[T]{
  def getInputFrom[U <: Formula[T,U] : TypeTag](regionDef : U) : Option[T]
}

trait RandomGetter[S,T <: Formula[S,T]] {
  def get(from : T) : Option[S]
}

object RandomGetter {
  def getter[S : TypeTag,T <: Formula[S,T] : TypeTag] : RandomGetter[S,T] = typeOf[S] match {
    case s if s =:= typeOf[Int] => typeOf[T] match {
      case t if t  =:= typeOf[IntervalsDisjunct] => RandomIntGetter.asInstanceOf[RandomGetter[S,T]]
      case t if t <:< typeOf[IntegerPredicatesFormula] => ScalacheckPredicatesIntGetter.asInstanceOf[RandomGetter[S,T]]
    }
    case s if s =:= typeOf[(Int,Int)] => ScalacheckIntBoxGetter.asInstanceOf[RandomGetter[S,T]]
    case s if s =:= typeOf[Array[Int]] => SolverIntArrayGetter.asInstanceOf[RandomGetter[S,T]]
  }
}

object RandomIntGetter extends RandomGetter[Int,IntervalsDisjunct]{
  def selectInterval(ints : IntervalsDisjunct) : Interval =
  //  ints.intervals(Random.nextInt(ints.intervals.length))
  {
    val p = Random.nextDouble
    val count = ints.disjuncts.map(i => i.to.toDouble - i.from.toDouble + 1).sum
    val dist = ints.disjuncts.map(i => i -> (i.to.toDouble - i.from.toDouble + 1)/count)
    val it = dist.iterator
    var accum = 0.0
    while(it.hasNext) {
      val (item, itemProb) = it.next
      accum += itemProb
      if (accum >= p)
        return item
    }
    ???
  }

  private def nextLong(n : Long) : Long = {
    if ((n & -n) == n)  // i.e., n is a power of 2
      return Random.nextLong() & (n - 1)

    var bits : Long = -1
    var v : Long = -1
    do {
      bits = (Random.nextLong() << 1) >>> 1
      v = bits % n
    } while (bits-v+(n-1) < 0L);
    return v
  }

  override def get(from: IntervalsDisjunct): Option[Int] = {
    if (from.isEmpty)
      None
    else {
      val d = selectInterval(from)
      Some((d.from + nextLong(d.to.toLong - d.from.toLong + 1)).toInt)
    }
  }
}

/*object ScalacheckIntGetter extends RandomGetter[Int,Formula[Int,_,_]] {
  private val gen : Gen[Int] = Arbitrary.arbitrary[Int]
  private def g : Stream[Option[Int]] = Stream.cons(gen.sample,g)
  override def get(from: Formula[Int,_,_]): Option[Int] = g.flatten.dropWhile { n => !from.includes(n)}.headOption
}*/

object ScalacheckIntervalsIntGetter extends RandomGetter[Int,IntervalsDisjunct] {
  override def get(from: IntervalsDisjunct): Option[Int] = {
    //val d : Interval = Gen.oneOf(from.intervals).sample.get
    val d : Interval = RandomIntGetter.selectInterval(from)
    Gen.chooseNum(d.from,d.to).sample
  }
}

object ScalacheckPredicatesIntGetter extends RandomGetter[Int,IntegerPredicatesFormula] with LazyLogging{
  override def get(from: IntegerPredicatesFormula): Option[Int] = {
    if (from.isEmpty) return None

    for (i <- Range(0, 100)) {
      val x = Arbitrary.arbitrary[Int].sample.get
      if (from.includes(x))
        return Some(x)
    }
    val x = Solver.mkFreshConst("x",Solver.mkIntSort).asInstanceOf[IntExpr]
    val expr = from.asSolverExpr(x)
    val solver = Solver.mkIntSolver(x)
    solver.add(expr)
    logger.debug("Solver called from oracle: " + solver.toString)
    val res = solver.check match {
      case Status.UNKNOWN => {
        logger.error("get from region timed out on " + solver)
        throw new TimeoutException()
      }
      case Status.SATISFIABLE => {
        val model = solver.getModel
        val v = model.getConstInterp(x).asInstanceOf[IntNum].getInt
        model.dispose()
        Some(v)
      }
      case _ => None
    }

    solver.dispose()
    expr.dispose()
    x.dispose()
    res
  }
}

object ScalacheckIntBoxGetter extends RandomGetter[(Int,Int),IntegerBoxesFormula] {
  override def get(from: IntegerBoxesFormula): Option[(Int, Int)] = {
    if (from.isEmpty) return None

    for (i <- Range(0, 10000)) {
      val x = Arbitrary.arbitrary[(Int,Int)].sample.get
      if (from.includes(x))
        return Some(x)
    }

    BoxSolverOps.getValue(from)
  }
}

object ScalacheckIntArrayGetter extends RandomGetter[Array[Int],ArrayPredicatesFormula] {
  override def get(from: ArrayPredicatesFormula): Option[Array[Int]] = {
    val ar = Arbitrary.arbitrary[Array[Int]]

    (1 to 100).foreach{_ =>
      val sopt = ar.sample
      if (!sopt.isEmpty) {
        val s = sopt.get
        if (from.includes(s)) {
          return sopt
        }
      }
    }

    return None
  }
}
object SolverIntArrayGetter extends RandomGetter[Array[Int],ArrayPredicatesFormula] with LazyLogging {
  override def get(from: ArrayPredicatesFormula): Option[Array[Int]] = {
    val sc = ScalacheckIntArrayGetter.get(from)
    if (!sc.isEmpty) return sc

    val x = Solver.mkArray()
    val expr = from.asSolverExpr(x)
    val solver = Solver.mkArraySolver(x)
    solver.add(expr)
    logger.debug("Solver called from oracle: " + solver.toString)
    val res = solver.check match {
      case Status.UNKNOWN => {
        logger.error("get from region timed out on " + solver)
        throw new TimeoutException()
      }
      case Status.SATISFIABLE => {
        val model = solver.getModel
        val v = Solver.getArrayFromModel(x,model)
        model.dispose()
        Some(v)
      }
      case _ => None
    }

    solver.dispose()
    expr.dispose()
    x.dispose()
    res
  }
}

class SimpleIntegersOracle extends Oracle[Int]{

  private val seen = scala.collection.mutable.Set[Int]()

  override def getInputFrom[T <: Formula[Int,T] : TypeTag](regionDef: T): Option[Int] = {
    if (regionDef.isEmpty)
      None
    else {
      val sampleRegion : T = regionDef.except(seen)
      if (sampleRegion.isEmpty) None
      else {
        val x : Option[Int] = RandomGetter.getter[Int,T].get(sampleRegion)

        x match {
          case Some(y) => seen.add(y)
          case None => {}
        }
        x
      }
    }
  }
}