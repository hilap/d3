package phd.d3

import scala.util.{Failure, Success, Try}

abstract class Result;
case class Value[T](value : T) extends Result;
case class Values[T](values : List[T]) extends Result;
case object Filtered extends Result;
case class Error (errType: String) extends Result;
case object TimedOut extends Result;

object Result {
  def apply[S,T](in: S, func: (S) => T): Result = {
    val res : Try[T] = Try(func(in))
    //wrap for timeout value
    //At least with the default execution context, this is not good
    //since this is not at all abortable.
    //But at the moment, whatever.
    //      val f : Future[Seq[T]] = Future{func(Seq(in))}
    //
    //      try {
    //        val res : Try[Seq[T]] = Await.ready(f, //apply on single value
    //          5 seconds).value.get
    //        res match {
    //          case Success(x) => x
    //          case Failure(e) => return Error(e.getClass.getName)
    //        }
    //      }
    //      catch {
    //        case e1: scala.concurrent.TimeoutException => {
    //          return TimedOut
    //        }
    //        case e2 : scala.concurrent.ExecutionException => {
    //          return Error(e2.getCause.getClass.getName)
    //        }
    //        case e3 : Exception => throw e3
    //      }
    //    }

    res match {
      case Success(x) => Value(x)
      case Failure(ex) => Error(ex.getClass.getName)
    }
  }
}