package phd.d3.WhiteboxOracle

import main.scala.phd.d3.integers.IntegerBoxDNFWrapper
import org.scalacheck.{Gen, Arbitrary}
import phd.d3._
import phd.d3.integers.IntervalsDisjunct

import scala.reflect.runtime.universe._
import scalariform.lexer.Tokens
import scalariform.parser.ScalaParser

/**
 * Created by hila on 06/07/2015.
 */

class WhiteboxIntOracle(val code : String) extends Oracle[Int]{
  private val parser = ScalaParser.parse(code)
  val specialValues  = parser.map(p => p.tokens.filter(t => t.tokenType==Tokens.INTEGER_LITERAL || t.tokenType == Tokens.STRING_LITERAL).map(t => t.tokenType match {
    case Tokens.INTEGER_LITERAL => t.text.toInt
    case Tokens.STRING_LITERAL => t.text.substring(t.text.indexOf("\"") + 1,t.text.lastIndexOf('\"')).length
  }).toSet).getOrElse(Set())
  private val seen = scala.collection.mutable.Set[Int]()

  override def getInputFrom[U <: Formula[Int, U] : TypeTag](regionDef: U): Option[Int] = {
    if (regionDef.isEmpty)
      None
    else {
      val sampleRegion: U = regionDef.except(seen)
      if (sampleRegion.isEmpty) None
      else {
        val sv = specialValues.filter(sampleRegion.includes)
        val x: Option[Int] = if (sv.isEmpty) RandomGetter.getter[Int,U].get(sampleRegion) else Some(sv.head)
        x match {
          case Some(y) => seen.add(y)
          case None => {}
        }
        x
      }
    }
  }
}

class Whitebox2DOracle(val code : String) extends Oracle[(Int,Int)] {
  private val parser = ScalaParser.parse(code)
  val specialValues  = parser.map(p => p.tokens.filter(t => t.tokenType==Tokens.INTEGER_LITERAL).map(t => t.text.toInt).toSet).getOrElse(Set())
  val specialValueTuples : Set[(Int,Int)] = (for(x <- specialValues; y <- specialValues) yield (x,y)) ++ (1 to 100).flatMap{ _ =>
    val x = Arbitrary.arbitrary[Int].sample.get
    specialValues.flatMap(v => List((v,x),(x,v)))
  }
  private val seen = scala.collection.mutable.Set[(Int,Int)]()
  override def getInputFrom[U <: Formula[(Int,Int), U] : TypeTag](regionDef: U): Option[(Int,Int)] = {
    if (regionDef.isEmpty)
      return None

    val sampleRegion : U = regionDef.except(seen)
    if (sampleRegion.isEmpty)
      return None

    val x : Option[(Int,Int)] = {
      val sv = specialValueTuples.filter(sampleRegion includes _).headOption
      if (!sv.isEmpty) sv else RandomGetter.getter[(Int, Int), U].get(sampleRegion)
    }

    x match {
      case None => {}
      case Some(y) => seen.add(y)
    }
    x
  }
}

class WhiteBoxIntArrayOracle(val code : String) extends Oracle[Array[Int]] {
  private val parser = ScalaParser.parse(code)
  val specialValues  = parser.map(p => p.tokens.filter(t => t.tokenType==Tokens.INTEGER_LITERAL).map(t => t.text.toInt).toSet).getOrElse(Set())
  private val seen = scala.collection.mutable.Set[Array[Int]]()
  override def getInputFrom[U <: Formula[Array[Int], U] : TypeTag](regionDef: U): Option[Array[Int]] = {

    if (regionDef.isEmpty) return None

    val sampleRegion: U = regionDef.except(seen)
    if (sampleRegion.isEmpty) return None

    //none of those workd, get from solver
    val x: Option[Array[Int]] = RandomGetter.getter[Array[Int], U].get(sampleRegion)
    x match {
      case Some(y) => seen.add(y)
      case None => {}
    }
    x

  }
}