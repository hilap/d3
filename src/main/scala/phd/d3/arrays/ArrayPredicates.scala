package main.scala.phd.d3.arrays

import java.util.concurrent.TimeoutException

import com.microsoft.z3.{Status, ArrayExpr, BoolExpr, IntExpr}
import main.scala.phd.d3.arrays
import phd.d3.{Formula, DNF, Disjunct}
import phd.d3.integers._
import phd.d3.integers.Solver.ArrayConst

import scala.collection
import scala.collection.immutable

/**
 * Created by hila on 06/08/2015.
 */

abstract class ArrayPredicate {
  def holdsFor(arr : Array[Int]) : Boolean
  def asSolverExpr(arr: ArrayConst) : com.microsoft.z3.BoolExpr
  val isTop : Boolean

  def isEq(other: ArrayPredicate): Boolean
  def hash : Int
  override def equals(o : Any) : Boolean = o match {
    case other : ArrayPredicate => isEq(other)
    case _ => false
  }
  override def hashCode() = this.getClass.hashCode() ^ hash
  val canJoin : Boolean = true
}

case class ArrayCellPredicate(idx : Int, intPredicate : IntegerPredicates) extends ArrayPredicate {
  override def holdsFor(arr: Array[Int]): Boolean = idx < arr.length && intPredicate.includes(arr(idx))

  override def asSolverExpr(arr: ArrayConst): BoolExpr = Solver.mkArrayElementExpr(arr, idx, intPredicate.asSolverExpr)

  override val isTop: Boolean = intPredicate.isTop

  override def isEq(other: ArrayPredicate): Boolean = other match {
    case ArrayCellPredicate(idx, intPredicate) => idx == this.idx && intPredicate == this.intPredicate
    case _ => false
  }

  override def hash: Int = idx ^ intPredicate.hashCode()
}
case class ForallCellsPredicate(intPredicate : IntegerPredicates) extends ArrayPredicate {
  override def holdsFor(arr: Array[Int]): Boolean = arr.forall(x => intPredicate.includes(x))

  override def asSolverExpr(arr: ArrayConst): BoolExpr = Solver.mkArrayForeach(arr,intPredicate.asSolverExpr)

  override val isTop: Boolean = intPredicate.isTop

  override def isEq(other: ArrayPredicate): Boolean = other match {
    case ForallCellsPredicate(intPredicate) => intPredicate == this.intPredicate
    case _ => false
  }

  override def hash: Int = intPredicate.hashCode()
}

// Maybe for Exists I want to add some more IntegerPredicate instances, such as equals, since lt && gt no longer holds...
case class ExistsCellPredicate(intPredicate : IntegerPredicates) extends ArrayPredicate {
  override def holdsFor(arr: Array[Int]): Boolean = !arr.forall(x => !intPredicate.includes(x))

  override def asSolverExpr(arr: ArrayConst): BoolExpr = Solver.mkArrayExists(arr, intPredicate.asSolverExpr)

  override val isTop: Boolean = intPredicate.isTop

  override def isEq(other: ArrayPredicate): Boolean = other match {
    case ExistsCellPredicate(intPredicate) => this.intPredicate == intPredicate
    case _ => false
  }

  override def hash: Int = intPredicate.hashCode()
}

case class ArrayLengthPredicate(intPredicate : IntegerPredicates) extends ArrayPredicate {
  override def holdsFor(arr: Array[Int]): Boolean = intPredicate.includes(arr.length)

  override def asSolverExpr(arr: ArrayConst): BoolExpr = intPredicate.asSolverExpr(arr.len)

  override val isTop: Boolean = intPredicate.isTop

  override def isEq(other: ArrayPredicate): Boolean = other match {
    case ArrayLengthPredicate(intPredicate) => intPredicate == this.intPredicate
    case _ => false
  }

  override def hash: Int = intPredicate.hashCode()
}

case object ArrayIsEmpty extends ArrayPredicate {
  override def holdsFor(arr: Array[Int]): Boolean = arr.length == 0

  override def hash: Int = Array().hashCode()

  override def isEq(other: ArrayPredicate): Boolean = other eq this

  override def asSolverExpr(arr: ArrayConst): BoolExpr = Solver.mkEq(arr.len,Solver.mkInt(0))

  override val isTop: Boolean = false

  override val canJoin : Boolean = false
}

object ArrayPredicate {
  def join(p1: ArrayPredicate, p2: ArrayPredicate) : ArrayPredicate = {
    if (p1 == p2) return p1
    val top = new IntegerPredicates(List(GreaterThanEq(Int.MinValue),LessThanEq(Int.MaxValue)))
    p1 match {
      case ArrayIsEmpty => ???
      case ForallCellsPredicate(intPredicate1) => p2 match {
        case ArrayCellPredicate(idx, intPredicate2) => ExistsCellPredicate(intPredicate1.join(intPredicate2))
        case ForallCellsPredicate(intPredicate2) => ForallCellsPredicate(intPredicate1.join(intPredicate2))
        case ExistsCellPredicate(intPredicate2) => ExistsCellPredicate(intPredicate1 join intPredicate2)
        case ArrayLengthPredicate(intPredicate) => ForallCellsPredicate(top)
        case ArrayIsEmpty => ???
      }
      case ArrayCellPredicate(idx1, intPredicate1) => p2 match {
        case ArrayCellPredicate(idx2, intPredicate2) =>
          if (idx1 == idx2)
            ArrayCellPredicate(idx2,intPredicate1 join intPredicate2)
          else if (intPredicate1.predicates.toSet == intPredicate2.predicates.toSet)
            ExistsCellPredicate(intPredicate1)
          else ExistsCellPredicate(top)
        case ExistsCellPredicate(intPredicate2) =>
          if (intPredicate1.predicates.toSet == intPredicate2.predicates.toSet)
            ExistsCellPredicate(intPredicate1)
          else
            ExistsCellPredicate(top)
        case ArrayIsEmpty => ???
        case _ => join(p2,p1)
      }
      case ExistsCellPredicate(intPredicate1) => p2 match {
        case ExistsCellPredicate(intPredicate2) => ExistsCellPredicate(new IntegerPredicates(intPredicate1.predicates.filter(p => intPredicate2.predicates.contains(p))))
        case ArrayIsEmpty => ???
        case _ => join(p2,p1)
      }
      case ArrayLengthPredicate(intPredicate1) => p2 match {
        case ArrayLengthPredicate(intPredicate2) => ArrayLengthPredicate(intPredicate1.join(intPredicate2))
        case ArrayIsEmpty => ???
        case _ => ArrayLengthPredicate(top)
      }
    }
  }

  def minimize(predicates : Iterable[ArrayPredicate]) : Iterable[ArrayPredicate] = {
    val distinct = predicates.toSet
    distinct.groupBy(p => p.getClass).map(_._2).flatMap { l =>
      l.head match {
        case ArrayCellPredicate(_, _) =>
          l.groupBy(ac => ac match {
            case ArrayCellPredicate(idx, pred) => idx
          }).map(_._2.map(_.asInstanceOf[ArrayCellPredicate])).
            map(ls => ArrayCellPredicate(ls.head.idx, new IntegerPredicates(IntegerPredicates.minimize(ls.flatMap(ac => ac.intPredicate.predicates)))))
        case ForallCellsPredicate(_) => List(ForallCellsPredicate(new IntegerPredicates(IntegerPredicates.minimize(l.flatMap(fa => fa match {case ForallCellsPredicate(intPred) => intPred.predicates})))))
        case ArrayLengthPredicate(_) => List(ArrayLengthPredicate(new IntegerPredicates(IntegerPredicates.minimize(l.flatMap(al => al match {case ArrayLengthPredicate(intPred) => intPred.predicates})))))
        //case ArrayIsEmpty => l
        case _ => l
      }
    }
  }
}

class ArrayPredicates (preds : Seq[ArrayPredicate]) extends Disjunct[Array[Int],ArrayPredicates]{
  val predicates : Seq[ArrayPredicate] = ArrayPredicate.minimize(preds).toSeq
  override def toString = predicates.mkString("(", " && ", ")")
  override def includes(x: Array[Int]): Boolean = predicates.forall(p => p.holdsFor(x))

  override def isEquivalent(x: ArrayPredicates): Boolean = ???

  override val canJoin : Boolean = predicates.forall(p => p.canJoin)
  override def join(other: ArrayPredicates): ArrayPredicates = {
    /*val joint = predicates.filter(p => other.predicates.contains(p))

    /*val sep = val sep = (this.predicates.diff(other.predicates) ++ other.predicates.diff(this.predicates)).groupBy(p => p match {
      case ForallCellsPredicate(innerPred) => p.getClass.toString + innerPred.getClass.toString
      case ExistsCellPredicate(innerPred) => p.getClass.toString + innerPred.getClass.toString
      case
    })*/
    val diffs = (this.predicates.diff(other.predicates) ++ other.predicates.diff(this.predicates)).filter(p => p match {
      case x: ArrayLengthPredicate => true
      case x: ArrayCellPredicate => true
      case _ => false
    }).groupBy(p => p match {
      case x: ArrayLengthPredicate => -1
      case ArrayCellPredicate(idx,pred) => idx
    }).filter{_._2.length == 2}.map(_._2)

    val jointDiffs = diffs.flatMap{preds =>
      preds match {
        case ArrayLengthPredicate(p1) :: ArrayLengthPredicate(p2) :: rest => {
          val joint = p1.join(p2)
          if (joint.predicates.isEmpty) None
          else Some(ArrayLengthPredicate(joint))
        }
        case ArrayCellPredicate(i1,p1) :: ArrayCellPredicate(i2,p2) :: rest => {
          assert(i1 == i2)
          val joint = p1.join(p2)
          if (joint.predicates.isEmpty) None
          else Some(ArrayCellPredicate(i1,joint))
        }
        case _ => None
      }
    }

    new ArrayPredicates(joint ++ jointDiffs)*/
    val joinPreds : Seq[ArrayPredicate] = for (p1 <- this.predicates;
         p2 <- other.predicates;
         joint = ArrayPredicate.join(p1, p2);
         if !joint.isTop)
    yield joint

    new ArrayPredicates(joinPreds.distinct)
  }


  override def isEmpty: Boolean = {
    val arr = Solver.mkArray
    val expr = asSolverExpr(arr)
    val solver = Solver.mkArraySolver(arr)
    solver.add(expr)
    //logger.debug("Solver called from ArrayPredicates.isEmpty: " + solver.toString)
    val res = solver.check match {
      case Status.UNSATISFIABLE => true
      case Status.UNKNOWN => {
        //logger.error("isEmpty timed out on " + solver)
        throw new TimeoutException()
      }
      case _ => false
    }
    solver.dispose()
    expr.dispose()
    arr.dispose()
    res
  }

  def asSolverExpr(arr: ArrayConst): BoolExpr = Solver.mkAnd( predicates.map(p => p.asSolverExpr(arr)) : _*)

  override def subsetOf(rhs: ArrayPredicates): Boolean = ???

  override def subsetEq(rhs: ArrayPredicates): Boolean = ???
}

object ArrayPredicates {
  def apply(array : Array[Int]) = {
    val preds : List[ArrayPredicate] = if (array.length == 0)
      List(ArrayIsEmpty)
    else {
      val allArr = if (array.length <= 10)
        (0 until array.length).filter(i => !specialValues.contains(i)).map(i => ArrayCellPredicate(i, IntegerPredicates(array(i))))
      else Seq()
      val svs: Seq[ArrayPredicate] = specialValues.flatMap { v =>
        val optional: List[ArrayPredicate] = if (array.length > v && v >= 0) ArrayCellPredicate(v, IntegerPredicates(array(v))) :: Nil else Nil
        val vFacts: List[ArrayPredicate] = IntegerPredicates(v).predicates.flatMap(p => List(ForallCellsPredicate(new IntegerPredicates(List(p))), ExistsCellPredicate(new IntegerPredicates(List(p)))))
        (/*vFacts ++ */ optional)
      }
      val allMods: Seq[ArrayPredicate] = for (m <- 2 to 10; c <- 0 until m) yield /*List(*/ ForallCellsPredicate(new IntegerPredicates(List(EqualsMod(m, c)))) /*, ExistsCellPredicate(EqualsMod(m,c)))*/

      val max = if (array.length > 0) Some(array.max) else None
      val min = if (array.length > 0) Some(array.min) else None
      val minmaxPreds = //List(ForallCellsPredicate(max), ExistsCellPredicate(max), ForallCellsPredicate(min), ExistsCellPredicate(min))
        (max.map(m => List(ForallCellsPredicate(new IntegerPredicates(IntegerPredicates(m).predicates.filter(p => array.forall(c => p.holdsFor(c))))), ExistsCellPredicate(m))).getOrElse(List()) ++
          min.map(m => List(ForallCellsPredicate(new IntegerPredicates(IntegerPredicates(m).predicates.filter(p => array.forall(c => p.holdsFor(c))))), ExistsCellPredicate(m))).getOrElse(List())).
          filter(p => !p.isTop)


      val predicates = (allArr ++ svs ++ allMods ++ minmaxPreds).toSet.filter(p => p.holdsFor(array))
      val lengthDesc = //if (specialValues.contains(array.length)) {
        ArrayLengthPredicate(IntegerPredicates(array.length))


      predicates.toList :+ lengthDesc
    }
    new ArrayPredicates(preds)
  }
  var specialValues : Seq[Int] = Seq()

  implicit def concreteToPredicates(x : Array[Int]) : ArrayPredicates = ArrayPredicates(x)

}


abstract class ArrayPredicatesFormula extends Formula[Array[Int],ArrayPredicatesFormula]
{
  def asSolverExpr(x : Solver.ArrayConst) : com.microsoft.z3.BoolExpr

  override def ||(rhs: ArrayPredicatesFormula): ArrayPredicatesFormula = ArrayPredicatesOr(List(this,rhs))

  override def including(xs: Iterable[Array[Int]]): ArrayPredicatesFormula = ArrayPredicatesOr(xs.filter(x => !includes(x)).map(x => ArrayPredicatesEqConst(x)).toList :+ this)

  override def unary_! : ArrayPredicatesFormula = ArrayPredicatesNot(this)

  override def &&(rhs: ArrayPredicatesFormula): ArrayPredicatesFormula = ArrayPredicatesAnd(List(this,rhs))

  override def isEmpty: Boolean = {
    val x = Solver.mkArray()
    val expr = asSolverExpr(x)
    val solver = Solver.mkArraySolver(x)
    solver.add(expr)
    //logger.debug("Solver called from IntegerPredicatesFormula.isEmpty: " + solver.toString)
    val res = solver.check match {
      case Status.UNSATISFIABLE => true
      case Status.UNKNOWN => {
        //logger.error("isEmpty timed out on " + solver)
        throw new TimeoutException()
      }
      case _ => false
    }
    solver.dispose()
    expr.dispose()
    x.dispose()
    res
  }

  override def isTautology: Boolean = (!this).isEmpty

  override def except(xs: Iterable[Array[Int]]): ArrayPredicatesFormula = {
    val exclusionList = xs.filter(includes).map(x => ArrayPredicatesEqConst(x)).toList
    if (exclusionList.isEmpty)
      this
    else
      ArrayPredicatesAnd(List(this,ArrayPredicatesNot(ArrayPredicatesOr(exclusionList))))
  }
}

case class ArrayPredicatesDNFWrapper(val dnf : ArrayPredicatesDNF) extends ArrayPredicatesFormula {
  //returns true if x satisfies this formula
  override def includes(x: Array[Int]): Boolean = dnf.includes(x)

  override def asSolverExpr(x: ArrayConst): BoolExpr = dnf.asSolverExpr(x)
}

case class ArrayPredicatesOr(clauses : List[ArrayPredicatesFormula]) extends ArrayPredicatesFormula {
  //returns true if x satisfies this formula
  override def includes(x: Array[Int]): Boolean = !clauses.forall(c => !c.includes(x))

  override def asSolverExpr(x: ArrayConst): BoolExpr = Solver.mkOr(clauses.map(c => c.asSolverExpr(x)):_*)
}

case class ArrayPredicatesNot(inner : ArrayPredicatesFormula) extends ArrayPredicatesFormula {
  //returns true if x satisfies this formula
  override def includes(x: Array[Int]): Boolean = !inner.includes(x)

  override def asSolverExpr(x: ArrayConst): BoolExpr = Solver.mkNot(inner.asSolverExpr(x))
}

case class ArrayPredicatesAnd(clauses : List[ArrayPredicatesFormula]) extends ArrayPredicatesFormula {
  //returns true if x satisfies this formula
  override def includes(x: Array[Int]): Boolean = clauses.forall(c => c.includes(x))

  override def asSolverExpr(x: ArrayConst): BoolExpr = Solver.mkAnd(clauses.map(c => c.asSolverExpr(x)):_*)
}

case class ArrayPredicatesEqConst(arr : Array[Int]) extends ArrayPredicatesFormula {
  override def asSolverExpr(x: ArrayConst): BoolExpr = Solver.mkAnd(
     (0 until arr.length).map(i => Solver.mkEq(Solver.mkApp(x.arr, Solver.mkInt(i)), Solver.mkInt(arr(i)))) :+
      Solver.mkEq(x.len, Solver.mkInt(arr.length)) :_*
  )

  //returns true if x satisfies this formula
  override def includes(x: Array[Int]): Boolean = x == arr
}

class ArrayPredicatesDNF(disj : List[ArrayPredicates]) extends DNF[Array[Int], ArrayPredicates, ArrayPredicatesDNF, ArrayPredicatesFormula] {
  def asSolverExpr(const: ArrayConst): BoolExpr = Solver.mkOr(disjuncts.map(d => d.asSolverExpr(const)):_*)

  override val disjuncts: List[ArrayPredicates] = disj

  override def ||(rhs: ArrayPredicatesFormula): ArrayPredicatesFormula = rhs match {
    case ArrayPredicatesDNFWrapper(rhsInner) => ArrayPredicatesDNFWrapper(new ArrayPredicatesDNF(disjuncts ++ rhsInner.disjuncts))
    case _ => asFormula || rhs
  }

  override def including(xs: Iterable[Array[Int]]): ArrayPredicatesDNF = {
    val toAdd = xs.filter(x => !includes(x)).map(x => ArrayPredicates(x))
    if (toAdd.isEmpty) this
    else new ArrayPredicatesDNF(disjuncts ++ toAdd)
  }

  override def unary_! : ArrayPredicatesFormula = !asFormula

  override def &&(rhs: ArrayPredicatesFormula): ArrayPredicatesFormula = asFormula && rhs

  override def asFormula: ArrayPredicatesFormula = ArrayPredicatesDNFWrapper(this)

  override def except(xs: Iterable[Array[Int]]): ArrayPredicatesFormula = asFormula.except(xs)

  override def toString = disjuncts.mkString("("," || ",")")
}

object ArrayPredicatesDNF{
  def apply(list: List[ArrayPredicates]) = new ArrayPredicatesDNF(list)
  implicit def makeDNF(disjuncts : List[ArrayPredicates]) : ArrayPredicatesDNF = ArrayPredicatesDNF(disjuncts)
}

