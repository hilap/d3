package main.scala.phd.d3.integers

import java.util.concurrent.TimeoutException

import com.microsoft.z3.{IntNum, Status, BoolExpr, IntExpr}
import phd.d3.integers.{Solver, Interval}
import phd.d3.{Formula, DNF, Disjunct}

/**
 * Created by hila on 02/09/2015.
 */
class IntegerBox (val x : Interval, val y : Interval) extends Disjunct[(Int,Int),IntegerBox]{
  override def includes(xy: (Int, Int)): Boolean = x.includes(xy._1) && y.includes(xy._2)

  override def isEquivalent(other: IntegerBox): Boolean = x.isEquivalent(other.x) && y.isEquivalent(other.y)

  override def join(other: IntegerBox): IntegerBox = IntegerBox(x join other.x, y join other.y)

  override def subsetEq(rhs: IntegerBox): Boolean = x.subsetEq(rhs.x) && y.subsetEq(rhs.y)

  override def subsetOf(rhs: IntegerBox): Boolean = x.subsetOf(rhs.x) && y.subsetOf(rhs.y)

  override def isEmpty: Boolean = x.isEmpty || y.isEmpty

  override def cardinality(originatingSet : Set[IntegerBox]) : BigInt = x.cardinality(Set()) * y.cardinality(Set())

  override val canJoin: Boolean = true
  override def toString : String = s"{$x,$y}"
}

object IntegerBox {
  def forSolver(box: IntegerBox, x: IntExpr, y: IntExpr): BoolExpr = Solver.mkAnd(
    Solver.mkLe(x,Solver.mkInt(box.x.to)),
    Solver.mkLe(y,Solver.mkInt(box.y.to)),
    Solver.mkGe(x,Solver.mkInt(box.x.from)),
    Solver.mkGe(y,Solver.mkInt(box.y.from))
  )

  def apply(x : Interval, y : Interval) : IntegerBox= new IntegerBox(x,y)
  def apply(xy : (Int,Int)) : IntegerBox = IntegerBox(Interval.unitConstructor(xy._1),Interval.unitConstructor(xy._2))
  implicit def unitConstructor(xy : (Int,Int)) = IntegerBox(xy)
}
class IntegerBoxesDNF(disj: List[IntegerBox]) extends DNF[(Int,Int),IntegerBox,IntegerBoxesDNF,IntegerBoxesFormula] {
  override val disjuncts: List[IntegerBox] = disj

  override def ||(rhs: IntegerBoxesFormula): IntegerBoxesFormula = this.asFormula || rhs

  override def including(xs: Iterable[(Int, Int)]): IntegerBoxesDNF = {
    val notIncluded = xs.filter(x => !(this includes x)).map(x => IntegerBox(x))
    new IntegerBoxesDNF(disjuncts ++ notIncluded)
  }

  override def unary_! : IntegerBoxesFormula = !(this.asFormula)

  override def &&(rhs: IntegerBoxesFormula): IntegerBoxesFormula = this.asFormula && rhs

  override def asFormula: IntegerBoxesFormula = IntegerBoxDNFWrapper(this)

  override def except(xs: Iterable[(Int, Int)]): IntegerBoxesFormula = this.asFormula.except(xs)

  override def toString : String = disjuncts.mkString("("," || ",")")
}

object IntegerBoxesDNF {
  implicit def fromList(clauses : List[IntegerBox]) = new IntegerBoxesDNF(clauses)
}
abstract class IntegerBoxesFormula extends Formula[(Int,Int),IntegerBoxesFormula] {
  def asSolverExpr(x: IntExpr, y: IntExpr): BoolExpr

  override def ||(rhs: IntegerBoxesFormula): IntegerBoxesFormula = IntegerBoxOr(List(this,rhs))

  override def including(xs: Iterable[(Int, Int)]): IntegerBoxesFormula = IntegerBoxOr(List(new IntegerBoxesDNF(xs.map(x => IntegerBox(x)).toList).asFormula,this))

  override def unary_! : IntegerBoxesFormula = IntegerBoxNot(this)

  override def &&(rhs: IntegerBoxesFormula): IntegerBoxesFormula = IntegerBoxAnd(List(this,rhs))

  override def isTautology: Boolean = (!this).isEmpty

  override def except(xs: Iterable[(Int, Int)]): IntegerBoxesFormula = IntegerBoxAnd(List(this,IntegerBoxNot(new IntegerBoxesDNF(xs.map(x => IntegerBox(x)).toList).asFormula)))
}

case class IntegerBoxDNFWrapper(dnf : IntegerBoxesDNF) extends IntegerBoxesFormula {
  //returns true if x satisfies this formula
  override def includes(x: (Int, Int)): Boolean = dnf.includes(x)
  override def isEmpty: Boolean = dnf.isEmpty

  override def asSolverExpr(x: IntExpr, y: IntExpr): BoolExpr = Solver.mkOr(dnf.disjuncts.map(IntegerBox.forSolver(_,x,y)):_*)
}
case class IntegerBoxNot(inner : IntegerBoxesFormula) extends IntegerBoxesFormula {
  //returns true if x satisfies this formula
  override def includes(x: (Int, Int)): Boolean = !inner.includes(x)
  override def isEmpty: Boolean = BoxSolverOps.testEmpty(this)

  override def asSolverExpr(x: IntExpr, y: IntExpr): BoolExpr = Solver.mkNot(inner.asSolverExpr(x,y))
}
case class IntegerBoxAnd(and : List[IntegerBoxesFormula]) extends IntegerBoxesFormula {
  //returns true if x satisfies this formula
  override def includes(x: (Int, Int)): Boolean = and.forall(b => b.includes(x))
  override def isEmpty: Boolean = BoxSolverOps.testEmpty(this)

  override def asSolverExpr(x: IntExpr, y: IntExpr): BoolExpr = Solver.mkAnd(and.map(_.asSolverExpr(x,y)):_*)
}
case class IntegerBoxOr(or : List[IntegerBoxesFormula]) extends IntegerBoxesFormula {
  //returns true if x satisfies this formula
  override def includes(x: (Int, Int)): Boolean = or.exists(b => b.includes(x))
  override def isEmpty: Boolean = or.forall(b => b.isEmpty)

  override def asSolverExpr(x: IntExpr, y: IntExpr): BoolExpr = Solver.mkOr(or.map(_.asSolverExpr(x,y)):_*)
}

object BoxSolverOps {
  def testEmpty(f : IntegerBoxesFormula) : Boolean = getValue(f).isEmpty

  def getValue(f : IntegerBoxesFormula) : Option[(Int,Int)] = {
    val x = Solver.mkFreshConst("x",Solver.mkIntSort).asInstanceOf[IntExpr]
    val y = Solver.mkFreshConst("y",Solver.mkIntSort).asInstanceOf[IntExpr]
    val solver = Solver.mkIntSolver(x,y)
    val fexpr : BoolExpr = f.asSolverExpr(x,y)

    solver.add(fexpr)
    val res = solver.check match {
      case Status.UNSATISFIABLE => None
      case Status.UNKNOWN => throw new TimeoutException
      case Status.SATISFIABLE => {
        val m = solver.getModel
        val tup = Some((m.eval(x,false).asInstanceOf[IntNum].getInt,
          m.eval(y,false).asInstanceOf[IntNum].getInt))
        m.dispose()
        tup
      }
    }
    solver.dispose()
    fexpr.dispose()
    x.dispose()
    y.dispose()
    res
  }
}