package phd.d3.integers

import java.util.concurrent.TimeoutException

import com.microsoft.z3
import com.microsoft.z3.{IntExpr, BoolExpr, Status}
import com.typesafe.scalalogging.LazyLogging
import phd.d3.{Formula, DNF, Disjunct}


/**
 * Created by hila on 28/06/2015.
 */

abstract class IntegerPredicate{
  def isTop: Boolean

  def holdsFor(x : Int) : Boolean;
  //takes the constant to use in the expr, returns the predicate as a z3 expression
  def asSolverExpr(x : com.microsoft.z3.IntExpr) : com.microsoft.z3.BoolExpr
}

case class LessThanEq(c : Int) extends IntegerPredicate {
  override def holdsFor(x: Int): Boolean = x <= c
  override def asSolverExpr(x: z3.IntExpr): BoolExpr = Solver.mkLe(x,Solver.mkInt(c))

  override def isTop: Boolean = c == Int.MaxValue
}
case class GreaterThanEq(c : Int) extends IntegerPredicate {
  override def holdsFor(x: Int): Boolean = x >= c
  override def asSolverExpr(x: z3.IntExpr): BoolExpr = Solver.mkGe(x,Solver.mkInt(c))

  override def isTop: Boolean = c == Int.MinValue
}
case class EqualsMod(m : Int, c : Int) extends IntegerPredicate {
  override def holdsFor(x: Int): Boolean = ((x % m) + m) % m == c
  override def asSolverExpr(x: z3.IntExpr): BoolExpr = Solver.mkEq(
    Solver.mkMod(x,Solver.mkInt(m))
    ,Solver.mkInt(c)
  )

  override def isTop: Boolean = false
}

class IntegerPredicates(val predicates : List[IntegerPredicate]) extends Disjunct[Int,IntegerPredicates] with LazyLogging{
  def isTop: Boolean = predicates.forall(p => p.isTop)
  override def equals(o: Any) = o match {
    case other : IntegerPredicates => predicates.toSet == other.predicates.toSet
    case _ => false
  }

  implicit final class Model(self: Int) {
    def |=(p: IntegerPredicate): Boolean = p.holdsFor(self)
  }

  override def includes(x: Int): Boolean = predicates.forall(p => x |= p)

  override def isEquivalent(other: IntegerPredicates): Boolean = {
    val x = Solver.mkFreshConst("x",Solver.mkIntSort).asInstanceOf[IntExpr]
    val iff = Solver.mkIff(this.asSolverExpr(x),other.asSolverExpr(x))
    val solver = Solver.mkIntSolver(x)
    solver.add(iff)
    logger.debug("Sovler called from IntegerPredicates.isEquivalent: " + solver.toString)
    val res = solver.check() match {
      case Status.UNSATISFIABLE => false
      case Status.UNKNOWN => {
        logger.error("IFF came out unknown for "+ solver)
        throw new Exception("Iff could not be solved for")
      }
      case Status.SATISFIABLE => true
    }
    solver.dispose()
    iff.dispose()
    x.dispose()
    res
  }

  private def modsToFormula(preds : List[IntegerPredicate], x : IntExpr) : Option[BoolExpr] = {
    val mods = preds.filter{ _ match { case e : EqualsMod => true case _ => false}}
    if (!mods.isEmpty) Option(new IntegerPredicates(mods).asSolverExpr(x))
    else {
      if (preds.length == 2){
        preds match {
          case GreaterThanEq(c_1) :: LessThanEq(c_2) :: rest => if (c_1 == c_2) Some(Solver.mkEq(x,Solver.mkInt(c_1))) else None
          case LessThanEq(c_1) :: GreaterThanEq(c_2) :: rest => if (c_1 == c_2) Some(Solver.mkEq(x,Solver.mkInt(c_1))) else None
          case _ => None
        }
      }
      else None
    }
  }
  override def join(other: IntegerPredicates): IntegerPredicates = {

    val joint = this.predicates.intersect(other.predicates)

    val sep = (this.predicates.diff(other.predicates) ++ other.predicates.diff(this.predicates)).groupBy(p => p.getClass)

    val toKeep = sep.flatMap { ps =>
      //if something exists only on one side, kill it.
      if (ps._2.length == 1)
        None
      else {
        ps._2 match {
          case EqualsMod(_, _) :: rest => None
          case GreaterThanEq(c_1) :: GreaterThanEq(c_2) :: rest => Some(GreaterThanEq(Math.min(c_1, c_2)))
          case LessThanEq(c_1) :: LessThanEq(c_2) :: rest => Some(LessThanEq(Math.max(c_1, c_2)))
        }
      }
    }
//      //this entire segment of code is for predicates other than intervals x congruence. Keeping it for a rainy day.
//      .filter { p =>
//      //this ==> p holds && other ==> p holds
//      p match {
//        case em : EqualsMod => {
//          val thisFormula = modsToFormula(this.predicates,x)
//          val otherFormula = modsToFormula(other.predicates,x)
//          if (thisFormula.isEmpty || otherFormula.isEmpty)
//            false
//          else if (Solver.impliesInt(thisFormula.get,p.asSolverExpr(x),x) &&
//              Solver.impliesInt(otherFormula.get,p.asSolverExpr(x),x))
//            /*true*/???
//          else false
//        }
//        case _ => if (Solver.impliesInt(this.asSolverExpr(x), p.asSolverExpr(x), x) && Solver.impliesInt(other.asSolverExpr(x), p.asSolverExpr(x), x))
//          true
//        else false
//      }
//    }

    return new IntegerPredicates(joint ++ toKeep)
  }

  override def isEmpty: Boolean = predicates.isEmpty

  def asSolverExpr(x : com.microsoft.z3.IntExpr) : com.microsoft.z3.BoolExpr = {
    Solver.mkAnd(predicates.map(p => p.asSolverExpr(x)).toArray : _*)
  }

  override def toString : String = predicates.mkString("(","&&",")")

  override def subsetOf(rhs: IntegerPredicates): Boolean = (this.predicates != rhs.predicates) && subsetEq(rhs)

  override def subsetEq(rhs: IntegerPredicates): Boolean = {
    val x = Solver.mkFreshConst("x",Solver.mkIntSort).asInstanceOf[IntExpr]
    if (Solver.impliesInt(this.asSolverExpr(x),rhs.asSolverExpr(x), x))
      true
    else
      false
  }

  override def cardinality(originatingSet: Set[IntegerPredicates]): BigInt = {
    val from : Long = predicates.map(p => p match { case GreaterThanEq(c) => c case _ => Int.MinValue }).max
    val to : Long = predicates.map(p => p match { case LessThanEq(c) => c case _ => Int.MaxValue }).min


    val congruences : List[EqualsMod] = predicates.filter(p => p match {case a : EqualsMod => true case _ => false}).asInstanceOf[List[EqualsMod]]
    //meet any pair of congruences that can meet

    def gcd(a: Int, b: Int):Int=if (b==0) a.abs else gcd(b, a%b)
    def lcm(a: Int, b: Int)=(a*b).abs/gcd(a,b)
    def meet(lhs : EqualsMod, rhs : EqualsMod) : Option[EqualsMod] = {
      val m_gcd = gcd(lhs.m,rhs.m)
      if (lhs.c % m_gcd == rhs.c % m_gcd) {
        val new_m = lcm(lhs.m, rhs.m)
        val max_m = if (lhs.m > rhs.m) lhs else rhs
        val new_c = (0 until new_m/max_m.m).map(i => max_m.m * i + max_m.c).filter(i => lhs.holdsFor(i) && rhs.holdsFor(i))
        assert(new_c.length == 1)
        Some(EqualsMod(new_m, new_c.head))
      }
      else None
    }

    val meets = congruences.//map((c1,c2) => if (c1.c % gcd(c1.m,c2.m) == c2.c % gcd(c1.m,c2.m)) Some(EqualsMod(lcm(c1.m,c2.m), c2.c)) else None)
    foldLeft(List[EqualsMod]()) { (z, mod) =>
      val (meets, bottoms) = z.partition(m => !meet(m,mod).isEmpty)
      if (meets.isEmpty)
        z :+ mod
      else {
        meets.map(m => meet(m,mod)).flatten ++ bottoms
      }
    }.toSet

    //val congs = (meets ++ used.filter(u => !u._2).map(_._1))
    val div = meets.foldLeft(1)((b, em) => lcm(b,em.m))

    Math.ceil(Math.abs(to-from).toDouble/div).toLong + 1
  }
  override def hashCode : Int = predicates.hashCode()

  override val canJoin: Boolean = true
}
object IntegerPredicates {
  def apply(x : Int) : IntegerPredicates = new IntegerPredicates(
    List(GreaterThanEq(x),LessThanEq(x)) ++
    (2 to 10).map(i => EqualsMod(i, if (x >= 0) (x % i) else ((x % i) + i) % i))
  )
  implicit def concreteToPredicates(x : Int) : IntegerPredicates = IntegerPredicates(x)

  def minimize(predicates: Iterable[IntegerPredicate]): List[IntegerPredicate] = {
    predicates.groupBy(p => p.getClass).map(_._2).flatMap{ l => l.head match {
      case LessThanEq(_) => List(LessThanEq(l.map(p => p match {case LessThanEq(c) => c}).min))
      case GreaterThanEq(_) => List(GreaterThanEq(l.map(p => p match {case GreaterThanEq(c) => c}).max))
      case EqualsMod(_, _) => l.toSet
    }}.toList
  }
}

abstract class IntegerPredicatesFormula extends Formula[Int,IntegerPredicatesFormula] with LazyLogging{
  def asSolverExpr(x : com.microsoft.z3.IntExpr) : com.microsoft.z3.BoolExpr

  //returns true if x satisfies this formula
//  override def includes(x: Int): Boolean = {
//    val inclusion = IntegerPredicatesAnd(List(this, IntegerPredicatesEqConst(x)))
//    val c = Solver.mkFreshConst("x",Solver.mkIntSort).asInstanceOf[IntExpr]
//    val expr = inclusion.asSolverExpr(c)
//    val solver = Solver.mkSolver
//    solver.add(expr)
//    solver.check == Status.SATISFIABLE
//  }

  override def ||(rhs: IntegerPredicatesFormula): IntegerPredicatesFormula = IntegerPredicatesOr(List(this,rhs))

  override def including(xs: Iterable[Int]): IntegerPredicatesFormula = IntegerPredicatesOr(xs.filter(x => !includes(x)).map(x => IntegerPredicatesEqConst(x)).toList :+ this)

  override def unary_! : IntegerPredicatesFormula = IntegerPredicatesNot(this)

  override def &&(rhs: IntegerPredicatesFormula): IntegerPredicatesFormula = IntegerPredicatesAnd(List(this,rhs))

  override def isEmpty: Boolean = {
    val x = Solver.mkFreshConst("x",Solver.mkIntSort).asInstanceOf[IntExpr]
    val expr = asSolverExpr(x)
    val solver = Solver.mkIntSolver(x)
    solver.add(expr)
    logger.debug("Solver called from IntegerPredicatesFormula.isEmpty: " + solver.toString)
    val res = solver.check match {
      case Status.UNSATISFIABLE => true
      case Status.UNKNOWN => {
        logger.error("isEmpty timed out on " + solver)
        throw new TimeoutException()
      }
      case _ => false
    }
    solver.dispose()
    expr.dispose()
    x.dispose()
    res
  }

  override def isTautology: Boolean = (!this).isEmpty

  override def except(xs: Iterable[Int]): IntegerPredicatesFormula = {
    val exclusionList = xs.filter(includes).map(x => IntegerPredicatesEqConst(x)).toList
    if (exclusionList.isEmpty)
      this
    else
      IntegerPredicatesAnd(List(this,IntegerPredicatesNot(IntegerPredicatesOr(exclusionList))))
  }
}
case class IntegerPredicatesDNFWrapper(inner : IntegerPredicatesDNF) extends IntegerPredicatesFormula {
  override def asSolverExpr(x: IntExpr): BoolExpr = Solver.mkOr(inner.disjuncts.map(d => d.asSolverExpr(x)):_*)
  override def includes(x: Int): Boolean = inner.includes(x)
  override def isEmpty: Boolean = inner.isEmpty
}
case class IntegerPredicatesOr(cubes : List[IntegerPredicatesFormula])extends IntegerPredicatesFormula {
  override def asSolverExpr(x: IntExpr): BoolExpr = Solver.mkOr(cubes.map(c => c.asSolverExpr(x)):_*)
  override def includes(x: Int): Boolean = !cubes.forall(c => !c.includes(x))
}
case class IntegerPredicatesNot(inner : IntegerPredicatesFormula) extends IntegerPredicatesFormula {
  override def asSolverExpr(x: IntExpr): BoolExpr = Solver.mkNot(inner.asSolverExpr(x))
  override def includes(x: Int): Boolean = !inner.includes(x)
}
case class IntegerPredicatesAnd(clauses : List[IntegerPredicatesFormula])extends IntegerPredicatesFormula {
  override def asSolverExpr(x: IntExpr): BoolExpr = Solver.mkAnd(clauses.map(c => c.asSolverExpr(x)):_*)
  override def includes(x: Int): Boolean = clauses.forall(c => c.includes(x))
}
case class IntegerPredicatesEqConst(c : Int) extends IntegerPredicatesFormula {
  override def asSolverExpr(x: IntExpr): BoolExpr = Solver.mkEq(x,Solver.mkInt(c))
  override def includes(x: Int): Boolean = x == c
}

class IntegerPredicatesDNF(cubes : List[IntegerPredicates]) extends DNF[Int,IntegerPredicates,IntegerPredicatesDNF,IntegerPredicatesFormula] {
  override val disjuncts: List[IntegerPredicates] = cubes

  override def ||(rhs: IntegerPredicatesFormula): IntegerPredicatesFormula = {
    rhs match {
      case IntegerPredicatesDNFWrapper(inner) => IntegerPredicatesDNFWrapper(new IntegerPredicatesDNF(disjuncts ++ inner.disjuncts))
      case _ => IntegerPredicatesDNFWrapper(this) || rhs
    }
  }

  override def including(xs: Iterable[Int]): IntegerPredicatesDNF = {
    val toAdd = xs.filter(x => !includes(x)).map(x => IntegerPredicates(x))
    if (toAdd.isEmpty) this
    else IntegerPredicatesDNF(disjuncts ++ toAdd)
  }

  override def unary_! : IntegerPredicatesFormula = !IntegerPredicatesDNFWrapper(this)

  override def &&(rhs: IntegerPredicatesFormula): IntegerPredicatesFormula = IntegerPredicatesDNFWrapper(this) && rhs

  override def except(xs: Iterable[Int]): IntegerPredicatesFormula = {
    IntegerPredicatesDNFWrapper(this).except(xs)
  }

  override def asFormula: IntegerPredicatesFormula = IntegerPredicatesDNFWrapper(this)

  override def toString : String = disjuncts.mkString(" || ")
}

object IntegerPredicatesDNF {
  def apply(disjuncts : List[IntegerPredicates]) = new IntegerPredicatesDNF(disjuncts)
  implicit def makeDNF(disjuncts : List[IntegerPredicates]) : IntegerPredicatesDNF = IntegerPredicatesDNF(disjuncts)
  implicit def asFormulaWrapper(dnf : IntegerPredicatesDNF) : IntegerPredicatesFormula = dnf.asFormula
}