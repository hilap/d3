/**
 * Created by hila on 17/05/2015.
 */
package phd.d3.integers

import phd.d3
import phd.d3._


class Interval(val from: Int, val to: Int) extends /*Disjunct*/Hydrable[Int,Interval] with Ordered[Interval]{

  override def < (rhs : Interval) : Boolean = {
    if (from < rhs.from)
      return true
    else if (from == rhs.from)
      return to < rhs.to
    else return false
  }
  def contains(rhs : Interval): Boolean = {
    rhs.from >= from && rhs.to <= to
  }
  override def includes(x : Int) : Boolean = {
    x >= from && x <= to
  }
  override def isEquivalent(other : Interval) : Boolean = from == other.from && to == other.to

  override def isEmpty : Boolean = from > to

  def splice(ints: Set[Int]) : List[Interval] = {
    val sorted = ints.toList.sorted.filter(x => x >= from && x <= to)
    if (sorted.isEmpty) return List(this)

    val ret = sorted.tail.foldLeft((if (sorted.head == from) List() else List(Interval(from,sorted.head-1)),sorted.head)){
      (z,f) =>
        val (list,prev) = z
        if (prev + 1 > f - 1)
          (list,f)
        else
          (list :+ Interval(prev + 1, f - 1), f)
    }._1
    if (sorted.last == to) (ret) else (ret :+ Interval(sorted.last + 1,to))
  }
  override def toString = "[" + from + "," + to + "]"
  override def equals(o: Any) = o match {
    case that : Interval => from == that.from && to == that.to
    case _ => false
  }
  override def hashCode = from.hashCode() ^ to.hashCode()

  override def compare(that: Interval): Int = if (this == that) 0 else if (this < that) -1 else 1

  override def join(other: Interval): Interval = if (this.isBottom)
      other
    else if (other.isBottom)
      this
    else
      Interval(Math.min(from,other.from),Math.max(to,other.to))

  override def subsetOf(rhs: Interval): Boolean = rhs.from <= this.from && rhs.to >= this.to && (rhs.from != this.from || rhs.to != this.to)

  override def subsetEq(rhs: Interval): Boolean = rhs.from <= this.from && rhs.to >= this.to

  override def cardinality(originatingSet: Set[Interval]): BigInt = (BigInt(to) - BigInt(from)).abs + 1

  override val canJoin: Boolean = true

  override def meet(other: Interval): Interval = Interval(Math.max(other.from,this.from),Math.min(other.to,this.to))

  override def isBottom: Boolean = this.to < this.from

  override def =<(other: Interval): Boolean = other.contains(this)
}
object Interval {
  def apply(from: Int, to: Int) = new Interval(from,to)
  def unapply(i : Interval) : Option[(Int,Int)] = Some((i.from,i.to))
  implicit def unitConstructor(x : Int) : Interval = Interval(x,x)
  implicit def alphaMinus(cs : Set[Int]) = Interval(Int.MinValue,Int.MaxValue).splice(cs)
}

class IntervalsDisjunct(intervals: List[Interval]) extends HydrableDNF[Int,Interval,IntervalsDisjunct,IntervalsDisjunct] with d3.Formula[Int,IntervalsDisjunct]{
  //returns true if x satisfies this formula
  override def includes(x: Int): Boolean = !intervals.filter(i => i includes x).isEmpty

  override def isEmpty: Boolean = intervals.isEmpty

  override def isTautology : Boolean = (!this).isEmpty

  override def except(xs: Iterable[Int]) : IntervalsDisjunct = {
    //do something to the inervals
    val intervalsExcept = intervals.flatMap { i =>
      val splicePoints = xs.filter(x => i.includes(x)).toSet
      if (splicePoints.isEmpty)
        Traversable(i)
      else
        i.splice(splicePoints)
    }
    new IntervalsDisjunct(intervalsExcept)
  }

  override def including(xs: Iterable[Int]): IntervalsDisjunct = {
    if (xs.isEmpty) this
    else {
      val toAdd = xs.filter(x => !this.includes(x))
      new IntervalsDisjunct(intervals ++ toAdd.map(x => Interval(x, x)))
    }
  }

  def ||(rhs: IntervalsDisjunct): IntervalsDisjunct = {
    val newIntervals = (disjuncts ++ rhs.disjuncts).toList.sorted.foldLeft(List[Interval]()) { (z, f) =>
      if (z.isEmpty) List(f)
      else {
        val prev = z.last
        if (prev contains f)
          z
        else if (f contains prev)
          z.init :+ f
        else if ((prev.to + 1 == f.from) || (prev includes f.from))
          z.init :+ Interval(prev.from, f.to)
        else z :+ f
      }
    }
    new IntervalsDisjunct(newIntervals)
  }

  override def unary_! : IntervalsDisjunct = {
    val std = intervals.sorted
    val inter = if (std.isEmpty) List(Interval(Int.MinValue,Int.MaxValue))
    else {
      val beforeLast = std.tail.foldLeft(
        (if (std.head.from == Int.MinValue) List() else List(Interval(Int.MinValue,std.head.from - 1)),std.head))
        { (z,f) =>
          if (f.from == Int.MinValue)
            (z._1,f)
          else {
            val lst = z._1
            val prev = z._2
            if (prev.to + 1 <= f.from - 1)
              (lst :+ Interval(prev.to +1, f.from -1),f)
            else
              (lst,f)
          }
        }._1
      if (std.last.to < Int.MaxValue) beforeLast :+ Interval(std.last.to + 1, Int.MaxValue) else beforeLast
    }
    new IntervalsDisjunct(inter)
  }


  override def &&(rhs: IntervalsDisjunct): IntervalsDisjunct = (for(x <- this.disjuncts; y <- rhs.disjuncts) yield x meet y).filter(!_.isBottom)

  override def toString = intervals.mkString("{",",","}")

  override val disjuncts: List[Interval] = intervals

  override def asFormula: IntervalsDisjunct = this
}
object IntervalsDisjunct {
  def apply(ds : List[Interval]) = new IntervalsDisjunct(ds)
  implicit def fromList(ds : List[Interval]): IntervalsDisjunct = IntervalsDisjunct(ds)
}