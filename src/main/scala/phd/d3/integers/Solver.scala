package phd.d3.integers

import java.util.concurrent.TimeoutException

import com.microsoft.z3._
import com.typesafe.scalalogging.LazyLogging
import collection.JavaConversions._
import scala.concurrent.duration._

/**
 * Created by hila on 28/06/2015.
 */
object Solver extends com.microsoft.z3.Context(Map("MODEL" -> "true", "trace" -> "true")) with  LazyLogging{
  def mkIntSolver(constants: IntExpr*) = {
    val solver = mkSolver
    constants.foreach(c => solver.add(mkIntBounds(c)))
    val p = mkParams();
    p.add("soft_timeout",(3 minutes).toMillis.toInt)
    solver.setParameters(p)
    solver
  }

  private def mkDoesNotImply(a : com.microsoft.z3.BoolExpr, b : com.microsoft.z3.BoolExpr) : com.microsoft.z3.BoolExpr =
    mkAnd(a,mkNot(b))

  def mkIntBounds(x : IntExpr) = mkAnd(mkLe(x,mkInt(Int.MaxValue)),mkGe(x,mkInt(Int.MinValue)))

  def impliesInt(a : com.microsoft.z3.BoolExpr, b : com.microsoft.z3.BoolExpr, x : IntExpr) : Boolean = {
    val notImplies = mkDoesNotImply(a,b)
    val solver = Solver.mkIntSolver(x)
    solver.add(notImplies)
    logger.debug("Solver called from Solver.impliesInt: " + solver.toString)
    val res = solver.check match {
      case Status.UNSATISFIABLE => true
      case Status.UNKNOWN => {
        logger.error("impliesInt timed out on " + solver)
        throw new TimeoutException()
      }
      case _ => false
    }
    solver.dispose()
    notImplies.dispose()
    res
  }

  class ArrayConst(val len : IntExpr, val arr : FuncDecl) {
    def dispose() = {
      len.dispose()
      arr.dispose()
    }
  }

  def mkArray() : ArrayConst = {
    val len = mkFreshConst("len",mkIntSort).asInstanceOf[IntExpr]
    val arr = mkFreshFuncDecl("arr",Array(mkIntSort), mkIntSort)
    new ArrayConst(len,arr)
  }

  def mkArrayLenBounds(len: IntExpr, i : IntExpr): BoolExpr = mkAnd(mkLt(i,len), mkGe(i, mkInt(0)))

  def mkArrayForeach(arr : ArrayConst, foreachExpr : (IntExpr => BoolExpr) ) : BoolExpr = {
    val i = mkIntConst("i")
    val toQuantify = foreachExpr(mkApp(arr.arr,i).asInstanceOf[IntExpr])
    assert(!toQuantify.isQuantifier)
    mkForall(
      Array(i),
      mkImplies(mkArrayLenBounds(arr.len, i),
        toQuantify), 1, null, null, null, null
    )
  }

  def mkArrayExists(arr : ArrayConst, existsExpr : (IntExpr => BoolExpr)) : BoolExpr = {
    val i = mkIntConst("i")
    val toQuantify = existsExpr(mkApp(arr.arr,i).asInstanceOf[IntExpr])
    assert(!toQuantify.isQuantifier)
    mkExists(
      Array(i),
      mkAnd(mkArrayLenBounds(arr.len,i),
        toQuantify
      ),
      1,null,null,null,null
    )
  }

  def mkArrayElementExpr(arr : ArrayConst, idx : Int, exprOnElement : (IntExpr => BoolExpr)) : BoolExpr = {
    val idxExpr = mkInt(idx).asInstanceOf[IntExpr]
    val elem = mkApp(arr.arr, idxExpr).asInstanceOf[IntExpr]
    val expr = exprOnElement(elem)
    mkAnd(
      mkArrayLenBounds(arr.len,idxExpr),
      expr
    )
  }

  def mkArraySolver(constants: ArrayConst*) = {
    val solver = mkSolver
    constants.foreach{c =>
      solver.add(mkAnd(mkGe(c.len, mkInt(0)), mkLe(c.len, mkInt(Int.MaxValue))))
      solver.add(mkArrayForeach(c, (a_i => mkIntBounds(a_i))))
    }
    val p = mkParams()
    p.add("soft_timeout",(3 minutes).toMillis.toInt)
    solver.setParameters(p)
    solver
  }

  def getArrayFromModel(arr : ArrayConst, m : Model) : Array[Int] = {
    val arrEval = m.getFuncInterp(arr.arr)
    val lenEval = m.eval(arr.len,false).asInstanceOf[IntNum]
    val lenInt = lenEval.getInt

    def makeArrayMap(arr : FuncInterp, model : Model) : (List[(Int, Int)], Int) = {
      val local = arr.getEntries.map { e =>
        val k = e.getArgs()(0).asInstanceOf[IntNum].getInt
        val v = e.getValue.asInstanceOf[IntNum].getInt
        k -> v
      }.toList
      if (arr.getElse.isIntNum) {
        val exprElse = arr.getElse.asInstanceOf[IntNum]
        val longElse = exprElse.getInt64
        //If the value is outside the range, it means it's also outside the range of length, since
        //there's a limit.
        val els = if (longElse <= Int.MaxValue && Int.MinValue <= longElse)
          longElse.toInt
        else
          -1
        return (local,els)
      }
      else {
        val (inner,els) = makeArrayMap(model.getFuncInterp(arr.getElse.getFuncDecl), model)
        return (local ++ inner, els)
      }
    }

    val (pairs, els) = makeArrayMap(arrEval, m)
    assert(pairs.filter(p => pairs.count(p2 => p2._1 == p._1) > 1).isEmpty)
    val arrMap = pairs.toMap

    val arrModel = (0 until lenInt).map(i => if (arrMap contains i) arrMap(i) else els).toArray
    arrModel
  }
}
