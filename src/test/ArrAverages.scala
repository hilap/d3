
object Find2 {
 def f1(x : Array[Int]) = x.indexOf(2)
  def f2(x : Array[Int]) : Int = {
    for (i <- 1 until x.length) {
		if (x(i) == 2)
			return i
	}
	return -1
  }
}