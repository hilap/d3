import scala.annotation.tailrec

object ArrayMaximum {
  def listMaximum(lst: Array[Int]) = {
    @tailrec def restOfListMinimum(curMax: Int, lst: Array[Int]): Int = {
      if (lst.isEmpty) curMax
      else restOfListMinimum(Math.max(curMax, lst(0)), lst.tail)
    }
    val max = lst(0)
    restOfListMinimum(max, lst.tail)
  }

  def listMaximumWrong(lst: Array[Int]): Int = {
    var max = 0
    lst.foreach { x =>
      max = if (max > x) max else x
    }
    return max
  }
}