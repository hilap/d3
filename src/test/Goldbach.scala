//keep for when I go back to dealing w/timeout
object TopMost{
object A {
  def getAllPrimes(upTo: Int) = {
    var res = List[Int]()
    var in = (2 to upTo).toList
    while (!in.isEmpty) {
      val x = in.head
      res = res :+ x
      in = in.tail.filter(_ % x != 0)
    }
    res
  }


  def goldbach(x: Int): Option[(Int, Int)] = {
    if (x % 2 == 1) return None
    val primes: List[Int] = getAllPrimes(x)
    val res = for (i <- primes; j <- primes.drop(i); if i + j == x) yield (i, j)
    Some(res.head)
  }
}

object B{
  def combinations[T](k: Int, list: List[T]) : List[List[T]] = {
    list match {
      case Nil => Nil
      case head :: xs =>
        if (k <= 0 || k > list.length) {
          Nil
        } else if (k == 1) {
          list.map(List(_))
        } else {
          combinations(k - 1, xs).map(head :: _) ::: combinations(k, xs)
        }
    }
  }
  def isPrime(i : Int) : Boolean = {
    if (i <= 1) false
    else if (i == 2) true
    else !(2 to (i - 1)).exists(x => i % x == 0)
  }
  def goldbach(i : Int) : Option[(Int,Int)]= {
    val primes = (3 to i).filter(x => isPrime(x)).toList
    combinations(2, primes).find(_.foldLeft(0)((a, b) => a + b) == i) match {
      case Some(list) => Some((list(0),list(1)))
      case None => throw new Exception("No goldbach sum for " + i)
    }
  }
}
}