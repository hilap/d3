//same

object HowManyBits {
  def solA(x : Int) = {
    x.toBinaryString.foldLeft(0)((sum,c) => sum + c.toString.toInt)
  }

  def solB(x : Int) = {
    def msks(x : Int) : Stream[Int] = Stream.cons(x, msks(x << 1))
    val masks = msks(1) take 32
    val bits = for (m <- masks) yield (x & m) != 0
    bits.count(b => b)
  }
}