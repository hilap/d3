
object Quadrant {

  def f1(x : Int, y : Int) : Int = {
    if (x >= 0)
      if (y >= 0) 1
      else 4
    else
      if (y >= 0) 2
      else 3
  }

  def f2(x: Int, y : Int) : Int = {
    if (x > 0 && y > 0) 1
    else if (y > 0) 2
    else if (x > 0) 4
    else 3
  }

}
