import scala.annotation.tailrec

//Same should be x % 10 = 0

object Input2 {
  def sumOfDigits(x : Int) : Int = {
    @tailrec def sumOfDigitsRec(sum : Int, rest : Int) : Int = {
      if (rest == 0) sum
      else sumOfDigitsRec(sum + rest % 10, rest/10)
    }
    sumOfDigitsRec(0,Math.abs(x))
  }

def sumOfDigitsWrong(x : Int) : Int = {
  var y = Math.abs(x)
  if (y < 10) y
  else {
    var sum = y % 10
    while (y > 0) {
      sum += y % 10
      y = y / 10
    }
    sum
  }
}
}