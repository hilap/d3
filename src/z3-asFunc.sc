import com.microsoft.z3.enumerations.Z3_decl_kind
import com.microsoft.z3._
import collection.JavaConversions._

val z = new Context(Map("MODEL" -> "true"))


//var arr = z.mkArrayConst("arr",z.mkIntSort(),z.mkIntSort())
var arr = z.mkFreshFuncDecl("arr",Array(z.mkIntSort), z.mkIntSort)
var len = z.mkIntConst("len")
val i = z.mkConst("i", z.mkIntSort).asInstanceOf[IntExpr]
val arrF = z.mkForall(
  Array(i),
  z.mkImplies(z.mkAnd(z.mkLt(i,len), z.mkGe(i, z.mkInt(0))),
    z.mkNot(z.mkEq(z.mkApp(arr,i), z.mkInt(0)))), 1, null, null, null, null
)
val arrE1 = z.mkExists(
  Array(i),
  z.mkAnd(z.mkLt(i,len), z.mkGe(i, z.mkInt(0)),
    z.mkEq(z.mkApp(arr,i),z.mkInt(100))
  ),
  1,null,null,null,null
)
val arrE2 = z.mkExists(
  Array(i),
  z.mkAnd(z.mkLt(i,len), z.mkGe(i, z.mkInt(0)),
    z.mkEq(z.mkApp(arr,i),z.mkInt(101))
  ),
  1,null,null,null,null
)
val a_8 = z.mkFreshConst("a_8", z.mkIntSort)
val specificIdx = z.mkAnd(
z.mkLe(z.mkApp(arr,z.mkInt(8)).asInstanceOf[IntExpr],z.mkInt(12)),
  z.mkGt(z.mkApp(arr,z.mkInt(8)).asInstanceOf[IntExpr],z.mkInt(6)),
z.mkGt(len,z.mkInt(8))
)


val arrSolver = z.mkSolver()

arrSolver.add(arrF)
arrSolver.add(arrE1)
arrSolver.add(z.mkNot(arrE2))
arrSolver.add(specificIdx)

arrSolver.check
val m = arrSolver.getModel
val arrEval = m.getFuncInterp(arr)
val lenEval = m.eval(len,false).asInstanceOf[IntNum]
val lenInt = lenEval.getInt
def makeArrayMap(arr : FuncInterp, model : Model) : (List[(Int, Int)], Int) = {
  val local = arr.getEntries.map { e =>
    val k = e.getArgs()(0).asInstanceOf[IntNum].getInt
    val v = e.getValue.asInstanceOf[IntNum].getInt
    k -> v
  }.toList
  println(local)
  if (arr.getElse.isIntNum) {
    return (local,arr.getElse.asInstanceOf[IntNum].getInt)
  }
  else {
    val (inner,els) = makeArrayMap(model.getFuncInterp(arr.getElse.getFuncDecl), model)
    return (local ++ inner, els)
  }
}

val (pairs, els) = makeArrayMap(arrEval, m)
assert(pairs.filter(p => pairs.count(p2 => p2._1 == p._1) > 1).isEmpty)
val arrMap = pairs.toMap

val arrModel = (0 until lenInt).map(i => if (arrMap contains i) arrMap(i) else els).toArray

//Array property fragment
