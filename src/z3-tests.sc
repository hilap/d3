import com.microsoft.z3._
import com.microsoft.z3.enumerations.{Z3_parameter_kind, Z3_decl_kind}
import collection.JavaConversions._
import scala.concurrent.duration._


//val cfg = new Z3Config("MODEL" -> true)
//cfg.toString
//val ctx = new Z3Context(cfg)
val z = new Context(Map("MODEL" -> "true"))
val x = z.mkFreshConst("x",z.mkIntSort())

val expr = z.mkGe(x.asInstanceOf[ArithExpr],z.mkInt(100))

val expr2 = z.mkLe(x.asInstanceOf[ArithExpr],z.mkInt(100))

val and = z.mkAnd(expr,expr2)

val impl = z.mkAnd(and,z.mkNot(z.mkEq(x,z.mkInt(110))))

val solver = z.mkSolver()
solver.add(impl)
solver.check()
val st = solver.getModel
st.getConstInterp(x).asInstanceOf[IntNum].getInt
//solver.getUnsatCore

val solver2 = z.mkSolver
solver2.add(z.mkNot(z.mkEq(x,z.mkInt(3))))
solver2.add(z.mkNot(z.mkEq(x,z.mkInt(4))))
solver2.add(z.mkNot(z.mkEq(x,z.mkInt(5))))
val range = z.mkAnd(
z.mkLe(x.asInstanceOf[ArithExpr],z.mkInt(5)),
z.mkGe(x.asInstanceOf[ArithExpr],z.mkInt(3))
)

solver2.add(range)
solver2.check()
//val a = z.mkGe(x.asInstanceOf[ArithExpr],z.mkInt(2))
//val b = z.mkGe(x.asInstanceOf[ArithExpr],z.mkInt(1))
//val expr3 = z.mkAnd(a,z.mkNot(b))
val solver3 = z.mkSolver
val xInt = x.asInstanceOf[IntExpr]
val mods = (2 to 10).map(i => (i, 816278040%i))
val expr3 = z.mkAnd(
z.mkAnd(
mods.map(p => z.mkEq(z.mkMod(xInt,z.mkInt(p._1)),z.mkInt(p._2))):_*)
, z.mkNot(z.mkEq(z.mkMod(xInt,z.mkInt(7)),z.mkInt(3))))
val p = z.mkParams()
p.add("soft_timeout",1)
solver3.setParameters(p)
solver3.add(expr3)
solver3.check()
//solver3.getModel
var arr = z.mkArrayConst("arr",z.mkIntSort(),z.mkIntSort())
var len = z.mkIntConst("len")
val i = z.mkFreshConst("i", z.mkIntSort).asInstanceOf[IntExpr]
val arrF = z.mkForall(
  Array(i),
  z.mkImplies(z.mkAnd(z.mkLt(i,len), z.mkGe(i, z.mkInt(0))),
    z.mkNot(z.mkEq(z.mkSelect(arr,i), z.mkInt(0)))), 1, null, null, null, null
)
val arrE1 = z.mkExists(
  Array(i),
  z.mkAnd(z.mkLt(i,len), z.mkGe(i, z.mkInt(0)),
    z.mkEq(z.mkSelect(arr,i),z.mkInt(100))
  ),
  1,null,null,null,null
)
val arrE2 = z.mkExists(
  Array(i),
  z.mkAnd(z.mkLt(i,len), z.mkGe(i, z.mkInt(0)),
    z.mkEq(z.mkSelect(arr,i),z.mkInt(101))
  ),
  1,null,null,null,null
)


val arrSolver = z.mkSolver()
val prms = z.mkParams();
prms.add("ematching",false)
solver.setParameters(prms)
arrSolver.add(arrF)
arrSolver.add(arrE1)
arrSolver.add(arrE2)
arrSolver.add(z.mkGt(len,z.mkInt(1)))
arrSolver.toString
arrSolver.check()
val m = arrSolver.getModel
val modelLen = m.getConstInterp(len).asInstanceOf[IntNum].getInt
//val arrayFunc = m.getFuncInterp(m.getConstDecls()(0)).
//val modelArr = (0 until modelLen).map(i => arrayFunc.)
val arrEval = m.eval(arr, false)
val arrFunc = arrEval.getFuncDecl
assert( arrFunc.getDeclKind() == Z3_decl_kind.Z3_OP_AS_ARRAY)
//assert( arrFunc.isApp)
//assert( arrFunc.getParameters()(0).getParameterKind() == Z3_parameter_kind.Z3_PARAMETER_FUNC_DECL)

val arrInterp : FuncInterp = m.getFuncInterp(m.getFuncDecls()(0))
val mp = arrInterp.getEntries.map { e =>
  val k = e.getArgs()(0).asInstanceOf[IntNum].getInt
  val v = e.getValue.asInstanceOf[IntNum].getInt
  k -> v
}.toMap
//val modelArr = (0 until modelLen).map{i =>
//  if (mp.contains(i))
//    mp(i)
//  else
//    arrInterp.getElse.asInstanceOf[IntNum].getInt
//}.toArray
val vrr = arrInterp.getElse.getArgs()(0).getFuncDecl.apply(z.mkInt(0)).getFuncDecl
val vrr2 = m.getFuncInterp(vrr).getElse.asInstanceOf[IntExpr]

def ExtractModelArrRec(expr : IntExpr, arrInterp : FuncInterp) = {
  val mp = arrInterp.getEntries.map { e =>
    val k = e.getArgs()(0).asInstanceOf[IntNum].getInt
    val v = e.getValue.asInstanceOf[IntNum].getInt
    k -> v
  }
  arrInterp.getElse
}